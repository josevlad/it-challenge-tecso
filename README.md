# IT Challenge Tecso

Bienvenidos al IT Challenge Tecso project

## Getting Started

Todos los detalles de la applicacion la puedes encontrar en la [wiki](https://gitlab.com/josevlad/it-challenge-tecso/wikis/home) siguiedo el paso a paso de las pruebas

### Prerequisites

Solo es necesario tener docker y docker-compose instalados (ver sitio web [docker](https://docs.docker.com/compose/install/))

## NEXT STEP

Ir a la [wiki](https://gitlab.com/josevlad/it-challenge-tecso/wikis/home)
