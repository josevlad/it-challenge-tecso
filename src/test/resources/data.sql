INSERT INTO country
  (country_code, name)
VALUES ('AR', 'ARGENTINA'),
       ('US', 'ESTADOS UNIDOS'),
       ('ES', 'ESPAÑA'),
       ('VE', 'VENEZUELA'),
       ('UR', 'URUGUAY');

INSERT INTO bank
  (bank_code, iba_code, name, country_id)
VALUES ('0001', 'AR01', 'BANCO NACION', 1),
       ('0002', 'US01', 'CITY BANK', 2),
       ('0003', 'ES01', 'BANCO SANTANDER RÍO', 3),
       ('0004', 'VE01', 'BANESCO', 4),
       ('0004', 'VE01', 'BANCO CIUDAD', 1);

INSERT INTO agency
  (agency_code, name, bank_id)
VALUES ('0001', 'VILLA CRESPO', 1),
       ('0002', 'FLORIDA', 1),
       ('0003', 'CABALLITO', 1);

INSERT INTO account_type
  (control_code, currency, name)
VALUES ('01', 'ARG', 'CUENTA CORRIENTE'),
       ('02', 'USD', 'CUENTA CORRIENTE'),
       ('03', 'EUR', 'CUENTA CORRIENTE');

INSERT INTO transaction_type
  (name)
VALUES ('CRÉDITO'),
       ('DÉBITO');

INSERT INTO customer
  (document_number, document_type, last_name, name)
VALUES ('95420300', 'DNI', 'CASTAÑEDA GALLEGO', 'VLADIMIR JOSE');


INSERT INTO bank_account
(account_code, account_number, balance, account_type_id, agency_id, customer_id)
VALUES ('0000000001', 'AR01-0001-0001-02-0000000001', 1103.00, 2, 1, 1);

INSERT INTO transaction
(amount, description, transaction_date, bank_account_id, transaction_type_id)
VALUES (275.75, 'Internet subscription payment', '2019-03-07 02:16:54.522000', 1, 1),
       (275.75, 'Internet subscription payment', '2019-03-07 02:17:12.973000', 1, 1),
       (275.75, 'Internet subscription payment', '2019-03-07 02:17:13.704000', 1, 1),
       (275.75, 'Internet subscription payment', '2019-03-07 02:17:14.663000', 1, 1);
