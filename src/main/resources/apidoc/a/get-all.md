# Get all Account Type

The response with status 200 (OK) and the list of Account Types in body or status 204 (No Content) and the empty array in body.

<style>
table.GeneratedTable {
  width: 100%;
  background-color: #ffffff;
border-collapse: separate;
  border-width: 2px;
  border-color: #000000;
  border-style: solid;
  color: #000000;
}

table.GeneratedTable td, table.GeneratedTable th {
  border-width: 2px;
  border-color: #000000;
  border-style: solid;
  padding: 5px;
}

table.GeneratedTable thead {
  background-color: #ffffff;
}
</style>

<table class="GeneratedTable">
  <thead>
    <tr>
      <th width="10%">CODE</th>
      <th width="20%">STATUS</th>
      <th width="70%">JSON RESPONSE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>200</td>
      <td>OK - list results</td>
      <td><pre>
[
  {
    "id": 1,
    "name": "CUENTA CORRIENTE",
    "currency": "ARG",
    "control_code": "01"
  },
  {
    "id": 2,
    "name": "CUENTA CORRIENTE",
    "currency": "USD",
    "control_code": "02"
  }
]
      </pre></td>
    </tr>
    <tr>
      <td>204</td>
      <td>No Content - empty array</td>
      <td><pre>
[]
      </pre></td>
    </tr>
  </tbody>
</table>
