# Delete Account Type by ID

The response with status 200 state (OK) with empty body or 409 If an error happens with the description of the error in the body.

<style>
table.GeneratedTable {
  width: 100%;
  background-color: #ffffff;
border-collapse: separate;
  border-width: 2px;
  border-color: #000000;
  border-style: solid;
  color: #000000;
}

table.GeneratedTable td, table.GeneratedTable th {
  border-width: 2px;
  border-color: #000000;
  border-style: solid;
  padding: 5px;
}

table.GeneratedTable thead {
  background-color: #ffffff;
}
</style>

<table class="GeneratedTable">
  <thead>
    <tr>
      <th width="10%">CODE</th>
      <th width="20%">STATUS</th>
      <th width="70%">JSON RESPONSE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>200</td>
      <td>OK - empty body</td>
      <td><pre></pre></td>
    </tr>
    <tr>
      <td>409</td>
      <td>Conflict - record not found to delete</td>
      <td><pre>
{
  "status": "CONFLICT",
  "errors": {
    "id": {
      "cant_delete_record_not_found": "can't delete the registry, non-existent record in the database"
    }
  }
}
      </pre></td>
    </tr>
  </tbody>
</table>
