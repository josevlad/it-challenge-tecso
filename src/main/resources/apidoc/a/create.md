# Create new Account Type

The Response with status 201 (Created) and with body the new accountTypeId, or with status 400 (Bad Request) if the accountTypeId exist by ID and name

<style>
table.GeneratedTable {
  width: 100%;
  background-color: #ffffff;
border-collapse: separate;
  border-width: 2px;
  border-color: #000000;
  border-style: solid;
  color: #000000;
}

table.GeneratedTable td, table.GeneratedTable th {
  border-width: 2px;
  border-color: #000000;
  border-style: solid;
  padding: 5px;
}

table.GeneratedTable thead {
  background-color: #ffffff;
}
</style>

<table class="GeneratedTable">
  <thead>
    <tr>
      <th width="10%">CODE</th>
      <th width="20%">STATUS</th>
      <th width="70%">JSON RESPONSE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>201</td>
      <td>Created</td>
      <td><pre>
{ 
  "id": 4, 
  "name": "CUENTA DE AHORROS", 
  "currency": "USD", 
  "control_code": 83 
}
      </pre></td>
    </tr>
    <tr>
      <td>400</td>
      <td>Bad Request - empty json post</td>
      <td><pre>
{
  "status": "BAD REQUEST",
  "errors": {
    "name": {
      "not_empty": "this field is required, ..."
    },
    "currency": {
      "not_empty": "this field is required, ..."
    }
  }
}
      </pre></td>
    </tr>
    <tr>
      <td>400</td>
      <td>Bad Request - invalid data</td>
      <td><pre>
{
  "status": "BAD REQUEST",
  "errors": {
    "name": {
      "only_letters": "valid only letters characters, ..."
    },
    "currency": {
      "enum_values": "this value not is one of the declared names, ..."
    }
  }
}
      </pre></td>
    </tr>
  </tbody>
</table>
