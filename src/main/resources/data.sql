INSERT INTO country
  (id, country_code, name)
VALUES (1, 'AR', 'ARGENTINA'),
       (2, 'US', 'ESTADOS UNIDOS'),
       (3, 'ES', 'ESPAÑA'),
       (4, 'VE', 'VENEZUELA'),
       (5, 'UR', 'URUGUAY');

INSERT INTO bank
  (id, bank_code, iba_code, name, country_id)
VALUES (1, '0001', 'AR01', 'BANCO NACION', 1),
       (2, '0002', 'US01', 'CITY BANK', 2),
       (3, '0003', 'ES01', 'BANCO SANTANDER RÍO', 3),
       (4, '0004', 'VE01', 'BANESCO', 4),
       (5, '0004', 'VE01', 'BANCO CIUDAD', 1);

INSERT INTO agency
  (id, agency_code, name, bank_id)
VALUES (1, '0001', 'VILLA CRESPO', 1),
       (2, '0002', 'FLORIDA', 1),
       (3, '0003', 'CABALLITO', 1);

INSERT INTO account_type
  (id, control_code, currency, name)
VALUES (1, '01', 'ARG', 'CUENTA CORRIENTE'),
       (2, '02', 'USD', 'CUENTA CORRIENTE'),
       (3, '03', 'EUR', 'CUENTA CORRIENTE');

INSERT INTO transaction_type
  (id, name)
VALUES (1, 'CRÉDITO'),
       (2, 'DÉBITO');

INSERT INTO customer
  (id, document_number, document_type, last_name, name)
VALUES (1, '95420300', 'DNI', 'CASTAÑEDA GALLEGO', 'VLADIMIR JOSE');

INSERT INTO bank_account
(id, account_code, account_number, balance, account_type_id, agency_id, customer_id)
VALUES (1, '0000000001', 'AR01-0001-0001-02-0000000001', 1103.00, 2, 1, 1);

INSERT INTO transaction
(id, amount, description, transaction_date, bank_account_id, transaction_type_id)
VALUES (1, 275.75, 'Internet subscription payment', '2019-03-07 02:16:54.522000', 1, 1),
       (2, 275.75, 'Internet subscription payment', '2019-03-07 02:17:12.973000', 1, 1),
       (3, 275.75, 'Internet subscription payment', '2019-03-07 02:17:13.704000', 1, 1),
       (4, 275.75, 'Internet subscription payment', '2019-03-07 02:17:14.663000', 1, 1);
