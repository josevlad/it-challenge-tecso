package es.coop.tecso.bank.service.impl;

import com.google.common.base.Strings;
import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.AgencyDTO;
import es.coop.tecso.bank.model.entity.Agency;
import es.coop.tecso.bank.model.entity.Bank;
import es.coop.tecso.bank.model.mapper.AgencyMapper;
import es.coop.tecso.bank.model.repository.AgencyRepository;
import es.coop.tecso.bank.model.repository.BankRepository;
import es.coop.tecso.bank.service.AgencyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;


@Service
@Transactional
public class AgencyServiceImpl implements AgencyService {

    private final Logger log = LoggerFactory.getLogger(AgencyServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("agencyRepository")
    private AgencyRepository agencyRepository;

    @Lazy
    @Autowired
    @Qualifier("bankRepository")
    private BankRepository bankRepository;

    private final AgencyMapper agencyMapper;

    public AgencyServiceImpl(AgencyMapper agencyMapper) {
        this.agencyMapper = agencyMapper;
    }

    /**
     * @param agencyDTO
     * @return
     */
    @Override
    public AgencyDTO save(AgencyDTO agencyDTO) throws StandardApplicationException {
        log.debug("Request to save Agency : {}", agencyDTO);
        Optional<Bank> optional = bankRepository.findByBankCode(agencyDTO.getBankCode());
        if (!optional.isPresent())
            throw new StandardApplicationException(
                    HttpStatus.CONFLICT,
                    new FieldError("agencyDTO", "bank_code", CANT_SAVE_OR_UPDATE_RECORD_NOT_FOUND));

        //TODO hacer validacion de nombre de agencia
        Agency agency = agencyMapper.toEntity(agencyDTO);
        setSequenceBankCode(agency);
        agency.setBank(optional.get());
        agency = agencyRepository.save(agency);
        return agencyMapper.toDto(agency);
    }

    /**
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<AgencyDTO> findAll() {
        log.debug("Request to get all Agencies");
        return agencyRepository.findAll().stream()
                .map(agencyMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<AgencyDTO> findAllByBankId(Long id) {
        log.debug("Request to get all Agencies by id Bank");
        return agencyRepository.findAllByBankId(id).stream()
                .map(agencyMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * @param id
     */
    @Override
    public void delete(Long id) throws StandardApplicationException {
        log.debug("Request to delete Agency : {}", id);
        FieldError fieldError = null;
        Optional<Agency> optionalAgencyToDelete = agencyRepository.findById(id);

        if (optionalAgencyToDelete.isPresent()) {
            if (optionalAgencyToDelete.get().getBankAccounts().size() == 0)
                agencyRepository.delete(optionalAgencyToDelete.get());
            else
                fieldError = new FieldError("agencyDTO", "id", CANT_DELETE_DEPENDENT_ENTITY);
        } else {
            fieldError = new FieldError("agencyDTO", "id", CANT_DELETE_RECORD_NOT_FOUND);
        }

        if (fieldError != null)
            throw new StandardApplicationException(HttpStatus.CONFLICT, fieldError);
    }

    /************************************************************************************************************/
    /**
     * @param agency
     */
    private void setSequenceBankCode(Agency agency) {
        Optional<Agency> optional = agencyRepository.findLastInsert();
        String zeros = "";
        String nextSequence = "0001";

        if (optional.isPresent()) {
            int lastSaved = Integer.parseInt(optional.get().getAgencyCode());
            nextSequence = String.valueOf(lastSaved + 1);
            if (String.valueOf(lastSaved).length() < 4)
                zeros = Strings.repeat("0", 4 - String.valueOf(lastSaved).length());
        }

        agency.setAgencyCode(zeros + nextSequence);
    }
}
