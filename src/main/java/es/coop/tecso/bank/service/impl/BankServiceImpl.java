package es.coop.tecso.bank.service.impl;

import com.google.common.base.Strings;
import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.BankDTO;
import es.coop.tecso.bank.model.entity.Bank;
import es.coop.tecso.bank.model.entity.Country;
import es.coop.tecso.bank.model.mapper.BankMapper;
import es.coop.tecso.bank.model.mapper.CountryMapper;
import es.coop.tecso.bank.model.repository.BankRepository;
import es.coop.tecso.bank.model.repository.CountryRepository;
import es.coop.tecso.bank.service.BankService;
import es.coop.tecso.bank.util.ServicesImplUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;


@Service
@Transactional
public class BankServiceImpl implements BankService {

    private final Logger log = LoggerFactory.getLogger(BankServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("bankRepository")
    private BankRepository bankRepository;

    @Autowired
    @Qualifier("countryRepository")
    private CountryRepository countryRepository;

    private final BankMapper bankMapper;

    private final CountryMapper countryMapper;

    public BankServiceImpl(BankMapper bankMapper, CountryMapper countryMapper) {
        this.bankMapper = bankMapper;
        this.countryMapper = countryMapper;
    }

    /**
     * Save a bank.
     *
     * @param bankDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public BankDTO save(BankDTO bankDTO) throws StandardApplicationException {
        log.debug("Request to save Bank : {}", bankDTO);
        Optional<Country> optional = countryRepository.findByCountryCode(bankDTO.getCountryCode());
        if (!optional.isPresent())
            throw new StandardApplicationException(
                    HttpStatus.CONFLICT,
                    new FieldError("bankDTO", "country_code", CANT_SAVE_OR_UPDATE_RECORD_NOT_FOUND));

        Optional<Bank> optionalBankLastInsert = bankRepository.findFirstByOrderByIdDesc();
        Bank bank = bankMapper.toEntity(bankDTO);

        if (optionalBankLastInsert.isPresent())
            setSequenceIbaCode(bankDTO, bank);

        bank.setCountry(optional.get());

        setSequenceBankCode(bank);

        bank = bankRepository.save(bank);
        return bankMapper.toDto(bank);
    }

    /**
     * Get all the banks.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<BankDTO> findAll() {
        log.debug("Request to get all Banks");
        return bankRepository.findAll().stream()
                .map(bankMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Delete the bank by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Bank : {}", id);
        bankRepository.deleteById(id);
    }

    /*************************************************************************************************/

    /**
     * @param bankDTO
     * @param bank
     * @throws StandardApplicationException
     */
    private void setSequenceIbaCode(BankDTO bankDTO, Bank bank) throws StandardApplicationException {
        Optional<Bank> optionalBank = bankRepository.findLastInsertByCountryCode(bankDTO.getCountryCode());
        String nextSequence;

        if (optionalBank.isPresent()) {
            String lastContryCodeSaved = optionalBank.get().getIbaCode().replaceAll("[^A-Z]", "");

            String bankDtoName = ServicesImplUtil.getNormalizerText(bankDTO.getName());
            String lastInsertByCountryCodeName = ServicesImplUtil.getNormalizerText(optionalBank.get().getName());

            if (lastInsertByCountryCodeName.equals(bankDtoName) && bankDTO.getCountryCode().equals(lastContryCodeSaved))
                throw new StandardApplicationException(
                        HttpStatus.CONFLICT,
                        new FieldError("bankDTO", "name", CANT_SAVE_OR_UPDATE_ASSIGNED_NAME));

            int lastSaved = Integer.parseInt(optionalBank.get().getIbaCode().replaceAll("[^0-9]", ""));
            nextSequence = (String.valueOf(lastSaved).length() == 1) ? "0" + (lastSaved + 1) : String.valueOf(lastSaved + 1);
        } else
            nextSequence = "01";

        bank.setIbaCode(bankDTO.getCountryCode() + nextSequence);
    }

    /**
     * @param bank
     */
    private void setSequenceBankCode(Bank bank) {
        Optional<Bank> optional = bankRepository.findLastInsert();
        String zeros = "";
        String nextSequence = "0001";

        if (optional.isPresent()) {
            int lastSaved = Integer.parseInt(optional.get().getBankCode());
            nextSequence = String.valueOf(lastSaved + 1);
            if (String.valueOf(lastSaved).length() < 4)
                zeros = Strings.repeat("0", 4 - String.valueOf(lastSaved).length());
        }

        bank.setBankCode(zeros + nextSequence);
    }
}
