package es.coop.tecso.bank.service.impl;

import com.google.common.base.Strings;
import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.AccountTypeDTO;
import es.coop.tecso.bank.model.entity.AccountType;
import es.coop.tecso.bank.model.mapper.AccountTypeMapper;
import es.coop.tecso.bank.model.repository.AccountTypeRepository;
import es.coop.tecso.bank.service.AccountTypeService;
import es.coop.tecso.bank.util.ServicesImplUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;


@Service
@Transactional
public class AccountTypeServiceImpl implements AccountTypeService {

    private final Logger log = LoggerFactory.getLogger(AccountTypeServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("accountTypeRepository")
    private AccountTypeRepository accountTypeRepository;

    private final AccountTypeMapper accountTypeMapper;

    public AccountTypeServiceImpl(AccountTypeMapper accountTypeMapper) {
        this.accountTypeMapper = accountTypeMapper;
    }

    /**
     * @param accountTypeDTO
     * @return
     */
    @Override
    public AccountTypeDTO save(AccountTypeDTO accountTypeDTO) throws StandardApplicationException {
        log.debug("Request to save AccountType : {}", accountTypeDTO);
        List<AccountType> byCurrency = accountTypeRepository.findByCurrency(accountTypeDTO.getCurrency());

        if (isSameAccountType(byCurrency, accountTypeDTO))
            throw new StandardApplicationException(
                    HttpStatus.CONFLICT,
                    new FieldError("accountTypeDTO", "a-data",
                            CANT_SAVE_OR_UPDATE_ASSIGNED_CODE));

        AccountType accountType = accountTypeMapper.toEntity(accountTypeDTO);
        setSequenceBankCode(accountType);

        accountType = accountTypeRepository.save(accountType);
        return accountTypeMapper.toDto(accountType);
    }

    /**
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<AccountTypeDTO> findAll() {
        log.debug("Request to get all AccountTypes");
        return accountTypeRepository.findAll().stream()
                .map(accountTypeMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * @param id
     */
    @Override
    public void delete(Long id) throws StandardApplicationException {
        log.debug("Request to delete AccountType : {}", id);
        FieldError fieldError = null;
        Optional<AccountType> optional = accountTypeRepository.findById(id);

        if (optional.isPresent()) {
            if (optional.get().getBankAccounts().size() == 0)
                accountTypeRepository.delete(optional.get());
            else
                fieldError = new FieldError("AccountType", "id", CANT_DELETE_DEPENDENT_ENTITY);
        } else {
            fieldError = new FieldError("AccountType", "id", CANT_DELETE_RECORD_NOT_FOUND);
        }

        if (fieldError != null)
            throw new StandardApplicationException(HttpStatus.CONFLICT, fieldError);
    }

    /***********************************************************************************************/

    private void setSequenceBankCode(AccountType bank) {
        Optional<AccountType> optional = accountTypeRepository.findLastInsert();
        String zeros = "";
        String nextSequence = "01";

        if (optional.isPresent()) {
            int lastSaved = Integer.parseInt(optional.get().getControlCode());
            nextSequence = String.valueOf(lastSaved + 1);
            if (String.valueOf(lastSaved).length() < 2)
                zeros = Strings.repeat("0", 2 - String.valueOf(lastSaved).length());
        }

        bank.setControlCode(zeros + nextSequence);
    }

    private boolean isSameAccountType(List<AccountType>  accountTypes, AccountTypeDTO dto) {
        return accountTypes.stream()
                .anyMatch(accountType ->
                        accountType.getCurrency().equals(dto.getCurrency()) &&
                        ServicesImplUtil.isSameString(accountType.getName(), dto.getName())
                );
    }
}
