package es.coop.tecso.bank.service;


import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.AgencyDTO;

import java.util.List;


public interface AgencyService {

    /**
     * @param agencyDTO
     * @return
     */
    AgencyDTO save(AgencyDTO agencyDTO) throws StandardApplicationException;

    /**
     * @return
     */
    List<AgencyDTO> findAll();

    /**
     * @param id
     * @return
     */
    List<AgencyDTO> findAllByBankId(Long id);

    /**
     * @param id
     */
    void delete(Long id) throws StandardApplicationException;
}
