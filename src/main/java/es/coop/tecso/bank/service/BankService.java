package es.coop.tecso.bank.service;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.BankDTO;

import java.util.List;

public interface BankService {

    /**
     * @param bankDTO
     * @return
     */
    BankDTO save(BankDTO bankDTO) throws StandardApplicationException;

    /**
     * @return
     */
    List<BankDTO> findAll();

    /**
     * @param id
     */
    void delete(Long id);
}
