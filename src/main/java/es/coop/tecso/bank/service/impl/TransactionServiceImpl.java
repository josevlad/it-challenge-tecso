package es.coop.tecso.bank.service.impl;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.TransactionDTO;
import es.coop.tecso.bank.model.entity.BankAccount;
import es.coop.tecso.bank.model.entity.Transaction;
import es.coop.tecso.bank.model.entity.TransactionType;
import es.coop.tecso.bank.model.mapper.TransactionMapper;
import es.coop.tecso.bank.model.repository.BankAccountRepository;
import es.coop.tecso.bank.model.repository.TransactionRepository;
import es.coop.tecso.bank.model.repository.TransactionTypeRepository;
import es.coop.tecso.bank.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.CANT_SAVE_OR_UPDATE_RECORD_NOT_FOUND;


@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

    private final Logger log = LoggerFactory.getLogger(TransactionServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("transactionRepository")
    private TransactionRepository transactionRepository;

    @Autowired
    @Qualifier("bankAccountRepository")
    private BankAccountRepository bankAccountRepository;

    @Autowired
    @Qualifier("transactionTypeRepository")
    private TransactionTypeRepository transactionTypeRepository;

    private final TransactionMapper transactionMapper;

    public TransactionServiceImpl(TransactionMapper transactionMapper) {
        this.transactionMapper = transactionMapper;
    }

    /**
     * @param transactionDTO
     * @return
     */
    @Override
    public TransactionDTO save(TransactionDTO transactionDTO, Long id) throws StandardApplicationException {
        log.debug("Request to save Transaction : {}", transactionDTO);
        Optional<BankAccount> optionalBankAccount = bankAccountRepository.findById(id);
        Optional<TransactionType> optionalTransactionType = transactionTypeRepository.findById(Long.parseLong(transactionDTO.getTransactionTypeId()));

        List<ObjectError> errorList = isNecessaryData(optionalBankAccount, optionalTransactionType);
        if (!errorList.isEmpty())
            throw new StandardApplicationException(HttpStatus.CONFLICT, errorList);

        Transaction transaction = transactionMapper.toEntity(transactionDTO)
                .bankAccount(optionalBankAccount.get())
                .transactionType(optionalTransactionType.get())
                .formatAmount()
                .updateBalancebankAccount();

        transaction = transactionRepository.save(transaction);
        return transactionMapper.toDto(transaction);
    }

    /**
     * @return
     * @param id
     */
    @Override
    @Transactional(readOnly = true)
    public List<TransactionDTO> findAllByBankAccountId(Long id) {
        log.debug("Request to get all Transactions");
        return transactionRepository.findAllByBankAccountIdOrderByTransactionDateDesc(id)
                .stream()
                .map(transactionMapper::toDto)
                .collect(Collectors.toList());
    }

    /********************************************************************************************************/

    private List<ObjectError> isNecessaryData(Optional<BankAccount> bankAccount, Optional<TransactionType> transactionType) {
        List<ObjectError> fieldErrors = new ArrayList<>();

        if (!bankAccount.isPresent())
            fieldErrors.add(new FieldError(
                    "bankAccountDTO",
                    "customer_document_number",
                    CANT_SAVE_OR_UPDATE_RECORD_NOT_FOUND));

        if (!transactionType.isPresent())
            fieldErrors.add(new FieldError(
                    "bankAccountDTO",
                    "transaction_Type",
                    CANT_SAVE_OR_UPDATE_RECORD_NOT_FOUND));

        return fieldErrors;
    }
}
