package es.coop.tecso.bank.service;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.CustomerDTO;
import es.coop.tecso.bank.model.dto.CustomerUpdateDTO;

import java.util.List;

public interface CustomerService {

    /**
     * @param customerDTO
     * @return
     */
    CustomerDTO save(CustomerDTO customerDTO) throws StandardApplicationException;

    /**
     * @param customerUpdateDTO
     * @param id
     * @return
     */
    CustomerUpdateDTO update(CustomerUpdateDTO customerUpdateDTO, Long id) throws StandardApplicationException;

    /**
     * @return
     */
    List<CustomerDTO> findAll();

    /**
     * @param id
     */
    void delete(Long id) throws StandardApplicationException;
}
