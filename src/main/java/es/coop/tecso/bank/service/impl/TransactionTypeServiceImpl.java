package es.coop.tecso.bank.service.impl;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.TransactionTypeDTO;
import es.coop.tecso.bank.model.dto.enums.TransactionTypeEnum;
import es.coop.tecso.bank.model.entity.TransactionType;
import es.coop.tecso.bank.model.mapper.TransactionTypeMapper;
import es.coop.tecso.bank.model.repository.TransactionTypeRepository;
import es.coop.tecso.bank.service.TransactionTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;


@Service
@Transactional
public class TransactionTypeServiceImpl implements TransactionTypeService {

    private final Logger log = LoggerFactory.getLogger(TransactionTypeServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("transactionTypeRepository")
    private TransactionTypeRepository transactionTypeRepository;

    private final TransactionTypeMapper transactionTypeMapper;

    public TransactionTypeServiceImpl(TransactionTypeMapper transactionTypeMapper) {
        this.transactionTypeMapper = transactionTypeMapper;
    }

    /**
     * @param transactionTypeDTO
     * @return
     */
    @Override
    public TransactionTypeDTO save(TransactionTypeDTO transactionTypeDTO) throws StandardApplicationException {
        log.debug("Request to save TransactionType : {}", transactionTypeDTO);
        TransactionTypeEnum transactionTypeEnum = TransactionTypeEnum.valueOf(transactionTypeDTO.getName());
        Optional<TransactionType> optional = transactionTypeRepository.findByName(transactionTypeEnum);

        if (optional.isPresent())
            throw new StandardApplicationException(
                    HttpStatus.CONFLICT,
                    new FieldError(
                            "transactionTypeDTO",
                            "name",
                            CANT_SAVE_OR_UPDATE_ALREADY_EXISTS));

        TransactionType transactionType = transactionTypeMapper.toEntity(transactionTypeDTO);
        transactionType = transactionTypeRepository.save(transactionType);
        return transactionTypeMapper.toDto(transactionType);
    }

    /**
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<TransactionTypeDTO> findAll() {
        log.debug("Request to get all TransactionTypes");
        return transactionTypeRepository.findAll().stream()
                .map(transactionTypeMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * @param id
     */
    @Override
    public void delete(Long id) throws StandardApplicationException {
        log.debug("Request to delete TransactionType : {}", id);
        FieldError fieldError = null;
        Optional<TransactionType> optional = transactionTypeRepository.findById(id);

        if (optional.isPresent()) {
            if (optional.get().getTransactions().size() == 0)
                transactionTypeRepository.delete(optional.get());
            else
                fieldError = new FieldError("TransactionType", "id", CANT_DELETE_DEPENDENT_ENTITY);
        } else {
            fieldError = new FieldError("TransactionType", "id", CANT_DELETE_RECORD_NOT_FOUND);
        }

        if (fieldError != null)
            throw new StandardApplicationException(HttpStatus.CONFLICT, fieldError);
    }
}
