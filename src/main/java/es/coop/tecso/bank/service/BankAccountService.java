package es.coop.tecso.bank.service;


import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.BankAccountCreateDTO;
import es.coop.tecso.bank.model.dto.BankAccountDTO;

import java.util.List;

public interface BankAccountService {

    /**
     * @param bankAccountDTO
     * @return
     */
    BankAccountDTO save(BankAccountCreateDTO bankAccountDTO) throws StandardApplicationException;

    /**
     * @return
     */
    List<BankAccountDTO> findAllByCustomerId(Long id);

    /**
     * @param id
     */
    void delete(Long id) throws StandardApplicationException;
}
