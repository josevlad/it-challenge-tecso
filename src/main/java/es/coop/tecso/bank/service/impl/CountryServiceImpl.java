package es.coop.tecso.bank.service.impl;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.CountryDTO;
import es.coop.tecso.bank.model.entity.Country;
import es.coop.tecso.bank.model.mapper.CountryMapper;
import es.coop.tecso.bank.model.repository.CountryRepository;
import es.coop.tecso.bank.service.CountryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;


@Service
@Transactional
public class CountryServiceImpl implements CountryService {

    private final Logger log = LoggerFactory.getLogger(CountryServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("countryRepository")
    private CountryRepository countryRepository;

    private final CountryMapper countryMapper;

    public CountryServiceImpl(CountryMapper countryMapper) {
        this.countryMapper = countryMapper;
    }

    /**
     * @param countryDTO
     * @return
     */
    @Override
    public CountryDTO save(CountryDTO countryDTO) throws StandardApplicationException {
        log.debug("Request to save Country : {}", countryDTO);
        Optional<Country> optional = countryRepository.findByCountryCode(countryDTO.getCountryCode());

        if (optional.isPresent() && optional.get().getId() != countryDTO.getId())
            throw new StandardApplicationException(
                    HttpStatus.CONFLICT,
                    new FieldError(
                            "countryDTO",
                            "country_code",
                            CANT_SAVE_OR_UPDATE_ASSIGNED_CODE));

        Country country = countryMapper.toEntity(countryDTO);
        country = countryRepository.save(country);
        return countryMapper.toDto(country);
    }

    /**
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<CountryDTO> findAll() {
        log.debug("Request to get all Countries");
        return countryRepository.findAll().stream()
                .map(countryMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * @param id
     */
    @Override
    public void delete(Long id) throws StandardApplicationException {
        log.debug("Request to delete Country : {}", id);
        FieldError fieldError = null;
        Optional<Country> optional = countryRepository.findById(id);

        if (optional.isPresent()) {
            if (optional.get().getBanks().size() == 0)
                countryRepository.delete(optional.get());
            else
                fieldError = new FieldError("Country", "id", CANT_DELETE_DEPENDENT_ENTITY);
        } else {
            fieldError = new FieldError("Country", "id", CANT_DELETE_RECORD_NOT_FOUND);
        }

        if (fieldError != null)
            throw new StandardApplicationException(HttpStatus.CONFLICT, fieldError);
    }
}
