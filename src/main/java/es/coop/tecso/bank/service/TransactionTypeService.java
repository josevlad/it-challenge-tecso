package es.coop.tecso.bank.service;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.TransactionTypeDTO;

import java.util.List;

public interface TransactionTypeService {

    /**
     * @param transactionTypeDTO
     * @return
     */
    TransactionTypeDTO save(TransactionTypeDTO transactionTypeDTO) throws StandardApplicationException;

    /**
     * @return
     */
    List<TransactionTypeDTO> findAll();

    /**
     * @param id
     */
    void delete(Long id) throws StandardApplicationException;
}
