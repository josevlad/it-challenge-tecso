package es.coop.tecso.bank.service;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.TransactionDTO;

import java.util.List;
import java.util.Optional;

public interface TransactionService {

    /**
     * @param transactionDTO
     * @return
     */
    TransactionDTO save(TransactionDTO transactionDTO, Long id) throws StandardApplicationException;

    /**
     * @param id
     * @return
     */
    List<TransactionDTO> findAllByBankAccountId(Long id);

}
