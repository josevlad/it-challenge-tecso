package es.coop.tecso.bank.service;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.CountryDTO;

import java.util.List;

public interface CountryService {

    /**
     * @param countryDTO
     * @return
     */
    CountryDTO save(CountryDTO countryDTO) throws StandardApplicationException;

    /**
     * @return
     */
    List<CountryDTO> findAll();

    /**
     * @param id
     */
    void delete(Long id) throws StandardApplicationException;
}
