package es.coop.tecso.bank.service;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.AccountTypeDTO;

import java.util.List;

public interface AccountTypeService {

    /**
     * @param accountTypeDTO
     * @return
     */
    AccountTypeDTO save(AccountTypeDTO accountTypeDTO) throws StandardApplicationException;

    /**
     * @return
     */
    List<AccountTypeDTO> findAll();

    /**
     * @param id
     */
    void delete(Long id) throws StandardApplicationException;
}
