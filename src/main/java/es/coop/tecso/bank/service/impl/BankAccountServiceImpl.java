package es.coop.tecso.bank.service.impl;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.BankAccountCreateDTO;
import es.coop.tecso.bank.model.dto.BankAccountDTO;
import es.coop.tecso.bank.model.entity.AccountType;
import es.coop.tecso.bank.model.entity.Agency;
import es.coop.tecso.bank.model.entity.BankAccount;
import es.coop.tecso.bank.model.entity.Customer;
import es.coop.tecso.bank.model.mapper.BankAccountMapper;
import es.coop.tecso.bank.model.mapper.CustomerMapper;
import es.coop.tecso.bank.model.repository.AccountTypeRepository;
import es.coop.tecso.bank.model.repository.AgencyRepository;
import es.coop.tecso.bank.model.repository.BankAccountRepository;
import es.coop.tecso.bank.model.repository.CustomerRepository;
import es.coop.tecso.bank.service.BankAccountService;
import es.coop.tecso.bank.util.ServicesImplUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;


@Service
@Transactional
public class BankAccountServiceImpl implements BankAccountService {

    private final Logger log = LoggerFactory.getLogger(BankAccountServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("bankAccountRepository")
    private BankAccountRepository bankAccountRepository;

    @Lazy
    @Autowired
    @Qualifier("customerRepository")
    private CustomerRepository customerRepository;

    @Lazy
    @Autowired
    @Qualifier("accountTypeRepository")
    private AccountTypeRepository accountTypeRepository;

    @Qualifier("agencyRepository")
    @Autowired
    private AgencyRepository agencyRepository;

    @Autowired
    private ServicesImplUtil servicesImplUtil;

    private final BankAccountMapper bankAccountMapper;

    private final CustomerMapper customerMapper;

    public BankAccountServiceImpl(BankAccountMapper bankAccountMapper, CustomerMapper customerMapper) {
        this.bankAccountMapper = bankAccountMapper;
        this.customerMapper = customerMapper;
    }

    /**
     * @param bankAccountDTO
     * @return
     */
    @Override
    public BankAccountDTO save(BankAccountCreateDTO bankAccountDTO) throws StandardApplicationException {
        log.debug("Request to save BankAccount : {}", bankAccountDTO);

        Optional<Customer> customer = customerRepository.findByDocumentNumber(bankAccountDTO.getCustomerDocumentNumber());
        Optional<AccountType> accountType = accountTypeRepository.findById(Long.parseLong(bankAccountDTO.getAccountTypeId()));
        Optional<Agency> agency = agencyRepository.findByAgencyCode(bankAccountDTO.getAgencyCode());

        List<ObjectError> necessaryData = isNecessaryData(customer, accountType, agency);
        if (!necessaryData.isEmpty())
            throw new StandardApplicationException(HttpStatus.CONFLICT, necessaryData);

        BankAccount bankAccount = new BankAccount()
                .customer(customer.get())
                .agency(agency.get())
                .accountType(accountType.get());

        bankAccount = servicesImplUtil.generateDataAccount(bankAccount);

        bankAccount = bankAccountRepository.saveAndFlush(bankAccount);
        return bankAccountMapper.toDto(bankAccount);
    }

    /**
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<BankAccountDTO> findAllByCustomerId(Long id) {
        log.debug("Request to get all BankAccounts");
        return bankAccountRepository.findByCustomerId(id).stream()
                .map(bankAccountMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * @param id
     */
    @Override
    public void delete(Long id) throws StandardApplicationException {
        log.debug("Request to delete BankAccount : {}", id);
        Optional<BankAccount> optionalBankAccount = bankAccountRepository.findById(id);
        FieldError fieldError = null;

        if (!optionalBankAccount.isPresent())
            fieldError = new FieldError( "bankAccountDTO", "bank_Account_id", CANT_DELETE_RECORD_NOT_FOUND);

        if (optionalBankAccount.isPresent() && optionalBankAccount.get().getTransactions().size() != 0)
            fieldError = new FieldError( "bankAccountDTO", "bank_Account_id", CANT_DELETE_DEPENDENT_ENTITY);

        if (fieldError != null)
            throw new StandardApplicationException(HttpStatus.CONFLICT, fieldError);

        bankAccountRepository.delete(optionalBankAccount.get());
    }

    /********************************************************************************************************/

    private List<ObjectError> isNecessaryData(Optional<Customer> customer, Optional<AccountType> accountType, Optional<Agency> agency) {
        List<ObjectError> fieldErrors = new ArrayList<>();

        if (!customer.isPresent())
            fieldErrors.add(new FieldError(
                    "bankAccountDTO",
                    "customer_document_number",
                    CANT_SAVE_OR_UPDATE_RECORD_NOT_FOUND));

        if (!accountType.isPresent())
            fieldErrors.add(new FieldError(
                    "bankAccountDTO",
                    "account_type",
                    CANT_SAVE_OR_UPDATE_RECORD_NOT_FOUND));

        if (!agency.isPresent())
            fieldErrors.add(new FieldError(
                    "bankAccountDTO",
                    "agency_code",
                    CANT_SAVE_OR_UPDATE_RECORD_NOT_FOUND));

        boolean isSameAccountType = false;
        if (customer.isPresent() && accountType.isPresent())
            isSameAccountType = customer.get().getBankAccounts().stream()
                    .anyMatch(bankAccount -> bankAccount.getAccountType().getId().equals(accountType.get().getId()));

        if (isSameAccountType)
            fieldErrors.add(new FieldError(
                    "bankAccountDTO",
                    "account_type",
                    CANT_SAVE_OR_UPDATE_ALREADY_EXISTS));

        return fieldErrors;
    }
}
