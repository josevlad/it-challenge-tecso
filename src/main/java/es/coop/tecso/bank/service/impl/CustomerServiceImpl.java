package es.coop.tecso.bank.service.impl;

import com.google.common.collect.Lists;
import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.CustomerDTO;
import es.coop.tecso.bank.model.dto.CustomerUpdateDTO;
import es.coop.tecso.bank.model.entity.AccountType;
import es.coop.tecso.bank.model.entity.Agency;
import es.coop.tecso.bank.model.entity.BankAccount;
import es.coop.tecso.bank.model.entity.Customer;
import es.coop.tecso.bank.model.mapper.CustomerMapper;
import es.coop.tecso.bank.model.mapper.CustomerUpdateMapper;
import es.coop.tecso.bank.model.repository.AccountTypeRepository;
import es.coop.tecso.bank.model.repository.AgencyRepository;
import es.coop.tecso.bank.model.repository.BankAccountRepository;
import es.coop.tecso.bank.model.repository.CustomerRepository;
import es.coop.tecso.bank.service.CustomerService;
import es.coop.tecso.bank.util.ServicesImplUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;


@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Value("${values.default.account-type}")
    private String accountTypeDefault;

    @Lazy
    @Autowired
    @Qualifier("customerRepository")
    private CustomerRepository customerRepository;

    @Lazy
    @Autowired
    @Qualifier("accountTypeRepository")
    private AccountTypeRepository accountTypeRepository;

    @Lazy
    @Autowired
    @Qualifier("agencyRepository")
    private AgencyRepository agencyRepository;

    @Lazy
    @Autowired
    @Qualifier("bankAccountRepository")
    private BankAccountRepository bankAccountRepository;

    private final CustomerMapper customerMapper;

    private final CustomerUpdateMapper customerUpdateMapper;

    @Autowired
    private ServicesImplUtil servicesImplUtil;

    public CustomerServiceImpl(CustomerMapper customerMapper, CustomerUpdateMapper customerUpdateMapper) {
        this.customerMapper = customerMapper;
        this.customerUpdateMapper = customerUpdateMapper;
    }

    /**
     * @param customerDTO
     * @return
     */
    @Override
    public CustomerDTO save(CustomerDTO customerDTO) throws StandardApplicationException {
        log.debug("Request to save Customer : {}", customerDTO);

        Optional<Agency> byAgencyCode = agencyRepository.findByAgencyCode(customerDTO.getAgencyCode());

        BankAccount bankAccount = getDataToCreateBanckAccount(byAgencyCode, customerDTO);

        bankAccount = servicesImplUtil.generateDataAccount(bankAccount);
        bankAccount = bankAccountRepository.save(bankAccount);
        bankAccount.customer(customerRepository.save(bankAccount.getCustomer()));
        Customer customer = bankAccount.getCustomer().addBankAccount(bankAccount);

        return customerMapper.toDto(customer);
    }

    @Override
    public CustomerUpdateDTO update(CustomerUpdateDTO customerUpdateDTO, Long id) throws StandardApplicationException {
        log.debug("Request to update Customer : {}", customerUpdateDTO);

        if (isAllFieldsNull(customerUpdateDTO))
            throw new StandardApplicationException(
                    HttpStatus.BAD_REQUEST,
                    new FieldError("customerDTO", "json_body", OBJECT_NULL));

        Optional<Customer> byId = customerRepository.findById(id);

        if (isNotSameCustomer(byId, customerUpdateDTO)) {
            Optional<Customer> byDocumentNumber = customerRepository.findByDocumentNumber(customerUpdateDTO.getDocumentNumber());
            if (byDocumentNumber.isPresent())
                throw new StandardApplicationException(
                        HttpStatus.BAD_REQUEST,
                        new FieldError("customerDTO", "document_number", CANT_SAVE_OR_UPDATE_ASSIGNED_ID));
        }

        Customer customer = customerUpdateMapper.toEntity(customerUpdateDTO);
        customer.setId(id);
        customer = customerRepository.save(customer);
        return customerUpdateMapper.toDto(customer);
    }

    /**
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<CustomerDTO> findAll() {
        log.debug("Request to get all Customers");
        return customerRepository.findAll().stream()
                .map(customerMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * @param id
     */
    @Override
    public void delete(Long id) throws StandardApplicationException {
        log.debug("Request to delete Customer : {}", id);
        FieldError fieldError = null;
        Optional<Customer> optional = customerRepository.findById(id);

        if (optional.isPresent()) {
            if (cantDeleteCustomer(optional.get())) {
                optional.get().getBankAccounts().forEach(bankAccount -> bankAccountRepository.delete(bankAccount));
                customerRepository.delete(optional.get());
            } else
                fieldError = new FieldError("Customer", "id", CANT_DELETE_DEPENDENT_ENTITY);
        } else {
            fieldError = new FieldError("AccountType", "id", CANT_DELETE_RECORD_NOT_FOUND);
        }

        if (fieldError != null)
            throw new StandardApplicationException(HttpStatus.CONFLICT, fieldError);
    }

    /**************************************************************************************************************/

    private boolean hasCustomerExist(CustomerDTO dto) {
        return customerRepository
                .findByDocumentNumber(dto.getDocumentNumber())
                .map(optional -> optional.getDocumentNumber().equals(dto.getDocumentNumber()))
                .orElse(false);
    }

    private boolean isNotSameCustomer(Optional<Customer> optionalCustomer, CustomerUpdateDTO dto) {
        return optionalCustomer
                .map(optional -> !optional.getDocumentNumber().equals(dto.getDocumentNumber()))
                .orElse(true);
    }

//    private AccountType getAccountTypeByNameOrId(String nameOrId) {
//        Optional<AccountType> accountType = Optional.empty();
//
//        if (nameOrId != null) {
//            if (nameOrId.chars().allMatch(Character::isDigit))
//                accountType = accountTypeRepository.findById(Long.parseLong(nameOrId));
//
//            else if (nameOrId.chars().allMatch(Character::isAlphabetic))
//                accountType = accountTypeRepository.findByName(nameOrId);
//        }
//        return accountType
//                .orElseGet(() -> accountTypeRepository.findById(Long.parseLong(accountTypeDefault)).get());
//
//    }

    private BankAccount getDataToCreateBanckAccount(Optional<Agency> agency, CustomerDTO customerDTO) throws StandardApplicationException {
        List<ObjectError> errors = Lists.newArrayList();

        if (!agency.isPresent())
            errors.add(new FieldError(
                    "agency",
                    "agency_code",
                    CANT_SAVE_OR_UPDATE_RECORD_NOT_FOUND));

        if (hasCustomerExist(customerDTO))
            errors.add(new FieldError(
                    "customerDTO",
                    "document_number",
                    CANT_SAVE_OR_UPDATE_ALREADY_EXISTS));

        if (!errors.isEmpty())
            throw new StandardApplicationException(HttpStatus.BAD_REQUEST, errors);

        AccountType accountType = servicesImplUtil.getAccountTypeByNameOrId(customerDTO.getAccountType());
        //getAccountTypeByNameOrId(customerDTO.getAccountTypeId());

        return new BankAccount()
                .customer(customerMapper.toEntity(customerDTO))
                .accountType(accountType)
                .agency(agency.get());
    }

//    private BankAccount generateDataAccount(BankAccount bankAccount) {
//        Optional<BankAccount> lastInsert = bankAccountRepository.findLastInsert();
//
//        String zeros = "";
//        String accountCode = "0000000001";
//        String accountNumber = bankAccount.getAgency().getBank().getIbaCode() + "-" +
//                bankAccount.getAgency().getBank().getBankCode() + "-" +
//                bankAccount.getAgency().getAgencyCode() + "-" +
//                bankAccount.getAccountTypeId().getControlCode();
//
//        if (lastInsert.isPresent()) {
//            int lastSaved = Integer.parseInt(lastInsert.get().getAccountCode());
//            accountCode = String.valueOf(lastSaved + 1);
//            if (String.valueOf(lastSaved).length() < 10)
//                zeros = Strings.repeat("0", 10 - String.valueOf(lastSaved).length());
//        }
//
//        accountCode = zeros + accountCode;
//
//        accountNumber = accountNumber + "-" + accountCode;
//
//        return bankAccount
//                .accountCode(accountCode)
//                .accountNumber(accountNumber)
//                .balanceDB(new BigDecimal("0.00"))
//                .formattBalance();
//    }

    private boolean cantDeleteCustomer(Customer customer) {
        return customer.getBankAccounts().stream()
                .allMatch(bankAccount -> bankAccount.getTransactions().size() == 0);
    }

    private boolean isAllFieldsNull(CustomerUpdateDTO dto) {
        return (dto.getName() == null || dto.getName().trim().isEmpty()) &&
                (dto.getLastName() == null || dto.getLastName().trim().isEmpty()) &&
                (dto.getDocumentNumber() == null || dto.getDocumentNumber().trim().isEmpty()) &&
                (dto.getDocumentType() == null || dto.getDocumentType().trim().isEmpty());
    }
}
