package es.coop.tecso.bank.advices;

import es.coop.tecso.bank.exception.StandardApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.NativeWebRequest;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestControllerAdvice({ "es.coop.tecso.bank.controller" })
public class ExceptionControllerHandling {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionControllerHandling.class);

    @ExceptionHandler(StandardApplicationException.class)
    public ResponseEntity handleStandardApplicationException(StandardApplicationException e, NativeWebRequest req) {
        HttpStatus httpStatus = e.getHttpStatus() != null ? e.getHttpStatus() : INTERNAL_SERVER_ERROR;
        return ResponseEntity.status(httpStatus).body(e.build());
    }
}
