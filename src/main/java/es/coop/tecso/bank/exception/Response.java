package es.coop.tecso.bank.exception;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

@SuppressWarnings("unchecked")
public class Response extends RestResponse implements Serializable {

    private static final long serialVersionUID = -6295324801768190127L;
    private Object data;

    public Response(HttpStatus httpStatus, Object data) {
        super(httpStatus, data);
        this.data = data;
    }
}
