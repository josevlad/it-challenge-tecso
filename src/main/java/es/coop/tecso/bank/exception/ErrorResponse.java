package es.coop.tecso.bank.exception;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

    private static final long serialVersionUID = -6295394701768190127L;
    private String status;
    private Object errors;

    public ErrorResponse(HttpStatus httpStatus, Object errors) {
        this.status = httpStatus.getReasonPhrase().toUpperCase();
        this.errors = errors;
    }

    public ErrorResponse() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getErrors() {
        return errors;
    }

    public void setErrors(Object errors) {
        this.errors = errors;
    }
}
