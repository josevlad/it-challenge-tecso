package es.coop.tecso.bank.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class RestResponse<T> implements Serializable {

    private static final long serialVersionUID = 3928270002376365195L;
    private String status;
    private T data;

    public RestResponse(HttpStatus httpStatus, T data) {
        this.status = httpStatus.getReasonPhrase();
        this.data = data;
    }
}
