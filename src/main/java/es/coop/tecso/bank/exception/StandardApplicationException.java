package es.coop.tecso.bank.exception;

import com.google.common.base.CaseFormat;
import com.google.common.collect.Lists;
import es.coop.tecso.bank.model.dto.enums.ValidationEnum;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StandardApplicationException extends Exception {

    private HttpStatus httpStatus;
    private List<ObjectError> bindingResult;

    public StandardApplicationException(String message) {
        super(message);
    }

    public StandardApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public StandardApplicationException(String message, Throwable cause, HttpStatus httpStatus) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }

    public StandardApplicationException(String message, Throwable cause, HttpStatus httpStatus, BindingResult bindingResult) {
        super(message, cause);
        this.httpStatus = httpStatus;
        this.bindingResult = bindingResult.getAllErrors();
    }

    public StandardApplicationException(HttpStatus httpStatus, BindingResult bindingResult) {
        this.httpStatus = httpStatus;
        this.bindingResult = bindingResult.getAllErrors();
    }

    public StandardApplicationException(HttpStatus httpStatus, FieldError fieldError) {
        this.httpStatus = httpStatus;
        this.bindingResult = Lists.newArrayList(fieldError);
    }

    public StandardApplicationException(HttpStatus httpStatus, List<ObjectError> errors) {
        this.httpStatus = httpStatus;
        this.bindingResult = errors;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    // Custom methods exception

    public boolean hasBindingResult() {
        return bindingResult != null;
    }

    public Map<String, Map<String, String>> getAllFieldErrors() {

        Map<String, Map<String, String>> listMapErrors = new HashMap<>();

        bindingResult.stream()
                .filter(FieldError.class::isInstance)
                .map(FieldError.class::cast)
                .collect(Collectors.toList())
                .forEach(fieldError -> {
                    String field = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, fieldError.getField());
                    String txtError = fieldError.getDefaultMessage();
                    String keyError = ValidationEnum.getFromMessages(txtError).name().toLowerCase();
                    listMapErrors.computeIfAbsent(field, ignoreMe -> new HashMap<>()).put(keyError, txtError);
                });

        return listMapErrors;
    }

    public Map<String, String> getErrorMap() {
        Map<String, String> errosMap = new HashMap<>();
        if (getMessage() != null) errosMap.put("error", getMessage());
        if (getCause() != null) errosMap.put("cause", String.valueOf(getCause()));
        if (httpStatus != null) errosMap.put("status", httpStatus.getReasonPhrase());
        return errosMap;
    }

    public ErrorResponse build() {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setStatus(getHttpStatus().getReasonPhrase().toUpperCase());
        if (hasBindingResult()) errorResponse.setErrors(getAllFieldErrors());
        else errorResponse.setErrors(getErrorMap());

        return errorResponse;
    }
}
