package es.coop.tecso.bank.controller;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.CustomerDTO;
import es.coop.tecso.bank.model.dto.CustomerUpdateDTO;
import es.coop.tecso.bank.service.CustomerService;
import es.coop.tecso.bank.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


@RestController
public class CustomerController extends MainRestController {

    private final Logger log = LoggerFactory.getLogger(CustomerController.class);

    private static final String ENTITY_CUSTOMER = "tecsoBankCustomer";

    @Autowired
    private CustomerService customerService;

    /**
     * POST  /customers : Create a new customer.
     *
     * @param customerDTO the customerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new customerDTO, or with status 400 (Bad Request) if the customer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customers")
    public ResponseEntity<CustomerDTO> createCustomer(
            @Valid @RequestBody(required = false) CustomerDTO customerDTO,
            BindingResult errors) throws StandardApplicationException, URISyntaxException {

        log.debug("REST request to save Customer : {}", customerDTO);
        if (customerDTO.getId() != null)
            errors.addError(new FieldError(
                    "customerDTO",
                    ENTITY_CUSTOMER + ".id",
                    "A new customer cannot already have an ID"));

        if (errors.hasErrors()) throw new StandardApplicationException(HttpStatus.BAD_REQUEST, errors);

        CustomerDTO result = customerService.save(customerDTO);
        return ResponseEntity.created(new URI("/api/customers/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_CUSTOMER, result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /customers/:id : Updates an existing customer.
     *
     * @param customerUpdateDTO the customerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated customerDTO,
     * or with status 400 (Bad Request) if the customerDTO is not valid,
     * or with status 500 (Internal Server Error) if the customerDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/customers/{id}")
    public ResponseEntity<CustomerUpdateDTO> updateCustomer(
            @PathVariable Long id,
            @Valid @RequestBody() CustomerUpdateDTO customerUpdateDTO,
            BindingResult errors) throws StandardApplicationException, URISyntaxException {

        log.debug("REST request to update Customer : {}", customerUpdateDTO);
        if (errors.hasErrors())
            throw new StandardApplicationException(HttpStatus.BAD_REQUEST, errors);

        CustomerUpdateDTO result = customerService.update(customerUpdateDTO, id);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_CUSTOMER, result.getId().toString()))
                .body(result);
    }

    /**
     * GET  /customers : get all the customers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of customers in body
     */
    @GetMapping("/customers")
    public List<CustomerDTO> getAllCustomers() {
        log.debug("REST request to get all Customers");
        return customerService.findAll();
    }

    /**
     * DELETE  /customers/:id : delete the "id" customer.
     *
     * @param id the id of the customerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/customers/{id}")
    public ResponseEntity<Void> deleteCustomer(@PathVariable Long id) throws StandardApplicationException {
        log.debug("REST request to delete Customer : {}", id);
        customerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_CUSTOMER, id.toString())).build();
    }
}
