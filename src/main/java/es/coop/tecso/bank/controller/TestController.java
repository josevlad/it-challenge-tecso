package es.coop.tecso.bank.controller;

import es.coop.tecso.bank.exception.StandardApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@RequestMapping("/api/test")
public class TestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    /**
     * @return
     */
    @GetMapping({ "/ping", "/ping/" })
    public ResponseEntity ping() {
        LOGGER.debug("REST request to ping test : {}", "pong");
        Map<String, String> pong = new HashMap<>();
        pong.put("result", "pong");
        return ResponseEntity.ok().body(pong);
    }

    /**
     * @return
     * @throws StandardApplicationException
     */
    @GetMapping({ "/server-test-error", "/server-test-error/" })
    public ResponseEntity<Object> serverErrorTest() throws StandardApplicationException {
        LOGGER.debug("REST request to server-error-test test : {}", "pong");
        try {
            Integer a = 0 / 0;
            return null;
        } catch (Exception e) {
            throw new StandardApplicationException(e.getMessage(), e.getCause(), BAD_REQUEST);
        }

    }
}
