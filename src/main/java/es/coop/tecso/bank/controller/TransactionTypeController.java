package es.coop.tecso.bank.controller;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.TransactionTypeDTO;
import es.coop.tecso.bank.service.TransactionTypeService;
import es.coop.tecso.bank.util.ControllerUtil;
import es.coop.tecso.bank.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


@RestController
public class TransactionTypeController extends MainRestController {

    private final Logger log = LoggerFactory.getLogger(TransactionTypeController.class);

    private static final String ENTITY_TRANSACTION_TYPE = "tecsoBankTransactionType";

    @Autowired
    private TransactionTypeService transactionTypeService;

    /**
     * POST  /transaction-types : Create a new transactionType.
     *
     * @param transactionTypeDTO the transactionTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new transactionTypeDTO, or with status 400 (Bad Request) if the transactionType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/transaction-types")
    public ResponseEntity<TransactionTypeDTO> createTransactionType(
            @Valid @RequestBody(required = false) TransactionTypeDTO transactionTypeDTO,
            BindingResult errors) throws StandardApplicationException, URISyntaxException {
        log.debug("REST request to save TransactionType : {}", transactionTypeDTO);
        if (transactionTypeDTO.getId() != null)
            errors.addError(new FieldError(
                    "countryDTO",
                    ENTITY_TRANSACTION_TYPE + ".id",
                    "A new country cannot already have an ID"));

        if (errors.hasErrors()) throw new StandardApplicationException(HttpStatus.BAD_REQUEST, errors);

        TransactionTypeDTO result = transactionTypeService.save(transactionTypeDTO);
        return ResponseEntity.created(new URI("/api/v1/transaction-types/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_TRANSACTION_TYPE, result.getId().toString()))
                .body(result);
    }

    /**
     * GET  /transaction-types : get all the transactionTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of transactionTypes in body
     */
    @GetMapping("/transaction-types")
    public ResponseEntity<?> getAllTransactionTypes() {
        log.debug("REST request to get all TransactionTypes");
        List<TransactionTypeDTO> all = transactionTypeService.findAll();
        HttpStatus status = (all.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return ControllerUtil
                .buildResponser(status, HeaderUtil.createAlert(ENTITY_TRANSACTION_TYPE, "all-transaction-types"), all);
    }

    /**
     * DELETE  /transaction-types/:id : delete the "id" transactionType.
     *
     * @param id the id of the transactionTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/transaction-types/{id}")
    public ResponseEntity<Void> deleteTransactionType(@PathVariable Long id) throws StandardApplicationException {
        log.debug("REST request to delete TransactionType : {}", id);
        transactionTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_TRANSACTION_TYPE, id.toString())).build();
    }
}
