package es.coop.tecso.bank.controller;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.AgencyDTO;
import es.coop.tecso.bank.service.AgencyService;
import es.coop.tecso.bank.util.ControllerUtil;
import es.coop.tecso.bank.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


@RestController
public class AgencyController extends MainRestController {

    private final Logger log = LoggerFactory.getLogger(AgencyController.class);

    private static final String ENTITY_AGENCY = "tecsoBankAgency";

    @Autowired
    private AgencyService agencyService;

    /**
     * POST  /agencies : Create a new agency.
     *
     * @param agencyDTO the agencyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new agencyDTO, or with status 400 (Bad Request) if the agency has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/agencies")
    public ResponseEntity<AgencyDTO> createAgency(
            @Valid @RequestBody AgencyDTO agencyDTO,
            BindingResult errors) throws URISyntaxException, StandardApplicationException {
        log.debug("REST request to save Agency : {}", agencyDTO);
        if (agencyDTO.getId() != null)
            errors.addError(new FieldError(
                    "agencyDTO",
                    ENTITY_AGENCY + ".id",
                    "A new agency cannot already have an ID"));

        if (errors.hasErrors()) throw new StandardApplicationException(HttpStatus.BAD_REQUEST, errors);

        AgencyDTO result = agencyService.save(agencyDTO);
        return ResponseEntity.created(new URI("/api/v1/agencies/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_AGENCY, result.getId().toString()))
                .body(result);
    }

    /**
     * GET  /agencies : get all the agencies.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of agencies in body
     */
    @GetMapping("/agencies")
    public ResponseEntity<?> getAllAgencies() {
        log.debug("REST request to get all Agencies");
        List<AgencyDTO> all = agencyService.findAll();
        HttpStatus status = (all.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return ControllerUtil
                .buildResponser(status, HeaderUtil.createAlert(ENTITY_AGENCY, "all-agencies"), all);
    }

    /**
     * GET  /banks/:id/agencies : return all agencies by the "id" bank.
     *
     * @param id the id of the bankDTO to find
     * @return the ResponseEntity with status 200 (OK) or status 204 (No Content) if there are no records
     */
    @GetMapping("/banks/{id}/agencies")
    public ResponseEntity<?> getAgenciesByCountryIdAndBankId(@PathVariable Long id) {
        log.debug("REST request to delete Agency : {}", id);
        List<AgencyDTO> all = agencyService.findAllByBankId(id);
        HttpStatus status = (all.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return ControllerUtil
                .buildResponser(status, HeaderUtil.createAlert(ENTITY_AGENCY, id.toString()), all);
    }

    /**
     * DELETE  /agencies/:id : delete the "id" agency.
     *
     * @param id the id of the agencyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping({"/agencies/{id}", "/agencies/{id}/"})
    public ResponseEntity<Void> deleteAgency(@PathVariable Long id) throws StandardApplicationException {
        log.debug("REST request to delete Agency : {}", id);
        agencyService.delete(id);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_AGENCY, id.toString()))
                .build();
    }
}
