package es.coop.tecso.bank.controller;

import es.coop.tecso.bank.config.swagger.ApiDocGeneral;
import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.AccountTypeDTO;
import es.coop.tecso.bank.service.AccountTypeService;
import es.coop.tecso.bank.util.ControllerUtil;
import es.coop.tecso.bank.util.HeaderUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.List;


@RestController
@Api(value = "Account Type Controller", description = "Transactions related to the Banking Accounts.")
public class AccountTypeController extends MainRestController {

    private final Logger log = LoggerFactory.getLogger(AccountTypeController.class);

    private static final String ENTITY_ACCOUNT_TYPE = "bankAccountType";

    //delete create get-all
    private String getTxtMD(String folder, String name) {
        File file = null;
        try {
            file = ResourceUtils.getFile("classpath:apidoc/" + folder + "/" + name + ".md");
            return new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Autowired
    private AccountTypeService accountTypeService;

    /**
     * POST  /account-types : Create a new accountType.
     *
     * @param accountTypeDTO the accountTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new accountTypeDTO,
     * or with status 400 (Bad Request) if the accountType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = ApiDoc.valueCreate, notes = ApiDoc.noteCreate, position = 0)
    @ApiResponses({
            @ApiResponse(code = 201, message = ApiDoc.CREATED, response = AccountTypeDTO.class),
            @ApiResponse(code = 400, message = ApiDoc.BAD_REQUEST)
    })
    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping("/account-type")
    public ResponseEntity<AccountTypeDTO> createAccountType(
            @Valid @RequestBody AccountTypeDTO accountTypeDTO,
            BindingResult errors) throws URISyntaxException, StandardApplicationException {

        log.debug("REST request to save AccountType : {}", accountTypeDTO);
        if (accountTypeDTO.getId() != null)
            errors.addError(new FieldError(
                    "accountTypeDTO",
                    ENTITY_ACCOUNT_TYPE + ".id",
                    "A new a cannot already have an ID"));

        if (errors.hasErrors()) throw new StandardApplicationException(HttpStatus.BAD_REQUEST, errors);

        AccountTypeDTO result = accountTypeService.save(accountTypeDTO);
        return ResponseEntity.created(new URI("/api/v1/countries/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_ACCOUNT_TYPE, result.getId().toString()))
                .body(result);
    }

    /**
     * GET  /account-types : get all the accountTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of accountTypes in body
     */
    @ApiOperation(value = ApiDoc.valueGetAll, notes = ApiDoc.noteGetAll, position = 1)
    @ApiResponses({
            @ApiResponse(code = 200, message = ApiDoc.CREATED, response = AccountTypeDTO.class, responseContainer = "List"),
    })
    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping("/account-type")
    public ResponseEntity<?> getAllAccountTypes() {
        log.debug("REST request to get all AccountTypes");
        List<AccountTypeDTO> all = accountTypeService.findAll();
        HttpStatus status = (all.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return ControllerUtil
                .buildResponser(status, HeaderUtil.createAlert(ENTITY_ACCOUNT_TYPE, "all-a"), all);
    }

    /**
     * DELETE  /account-types/:id : delete the "id" accountType.
     *
     * @param id the id of the accountTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK) or 409 If an error happens
     */
    @ApiOperation(value = ApiDoc.valueDelete, notes = ApiDoc.noteDelete, position = 2)
    @ResponseStatus(value = HttpStatus.OK)
    @DeleteMapping("/account-type/{id}")
    public ResponseEntity<Void> deleteAccountType(@PathVariable Long id) throws StandardApplicationException {
        log.debug("REST request to delete AccountType : {}", id);
        accountTypeService.delete(id);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_ACCOUNT_TYPE, id.toString()))
                .build();
    }

    // --------------- for Swagger Annotations

    private interface ApiDoc extends ApiDocGeneral {

        String valueCreate = "Create a new Account Type";

        String noteCreate = "# Create new Account Type\n" +
                "\n" +
                "The Response with status 201 (Created) and with body the new accountType, or with status 400 (Bad Request) if the accountType exist by ID and name\n<br>" +
                "\n" +
                "<style>\n" +
                "table.GeneratedTable {\n" +
                "  width: 100%;\n" +
                "  background-color: #ffffff;\n" +
                "border-collapse: separate;\n" +
                "  border-width: 2px;\n" +
                "  border-color: #000000;\n" +
                "  border-style: solid;\n" +
                "  color: #000000;\n" +
                "}\n" +
                "\n" +
                "table.GeneratedTable td, table.GeneratedTable th {\n" +
                "  border-width: 2px;\n" +
                "  border-color: #000000;\n" +
                "  border-style: solid;\n" +
                "  padding: 5px;\n" +
                "}\n" +
                "\n" +
                "table.GeneratedTable thead {\n" +
                "  background-color: #ffffff;\n" +
                "}\n" +
                "</style>\n" +
                "\n" +
                "<table class=\"GeneratedTable\">\n" +
                "  <thead>\n" +
                "    <tr>\n" +
                "      <th width=\"10%\">CODE</th>\n" +
                "      <th width=\"20%\">STATUS</th>\n" +
                "      <th width=\"70%\">JSON RESPONSE</th>\n" +
                "    </tr>\n" +
                "  </thead>\n" +
                "  <tbody>\n" +
                "    <tr>\n" +
                "      <td>201</td>\n" +
                "      <td>Created</td>\n" +
                "      <td><pre>\n" +
                "{ \n" +
                "  \"id\": 4, \n" +
                "  \"name\": \"CUENTA DE AHORROS\", \n" +
                "  \"currency\": \"USD\", \n" +
                "  \"control_code\": 83 \n" +
                "}\n" +
                "      </pre></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td>400</td>\n" +
                "      <td>Bad Request - empty json post</td>\n" +
                "      <td><pre>\n" +
                "{\n" +
                "  \"status\": \"BAD REQUEST\",\n" +
                "  \"errors\": {\n" +
                "    \"name\": {\n" +
                "      \"not_empty\": \"this field is required, ...\"\n" +
                "    },\n" +
                "    \"currency\": {\n" +
                "      \"not_empty\": \"this field is required, ...\"\n" +
                "    }\n" +
                "  }\n" +
                "}\n" +
                "      </pre></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td>400</td>\n" +
                "      <td>Bad Request - invalid data</td>\n" +
                "      <td><pre>\n" +
                "{\n" +
                "  \"status\": \"BAD REQUEST\",\n" +
                "  \"errors\": {\n" +
                "    \"name\": {\n" +
                "      \"only_letters\": \"valid only letters characters, ...\"\n" +
                "    },\n" +
                "    \"currency\": {\n" +
                "      \"enum_values\": \"this value not is one of the declared names, ...\"\n" +
                "    }\n" +
                "  }\n" +
                "}\n" +
                "      </pre></td>\n" +
                "    </tr>\n" +
                "  </tbody>\n" +
                "</table>\n";


        String valueGetAll = "Get all Account Type";

        String noteGetAll = "# Get all Account Type\n" +
                "\n" +
                "The response with status 200 (OK) and the list of Account Types in body or status 204 (No Content) and the empty array in body.\n<br>" +
                "\n" +
                "<style>\n" +
                "table.GeneratedTable {\n" +
                "  width: 100%;\n" +
                "  background-color: #ffffff;\n" +
                "border-collapse: separate;\n" +
                "  border-width: 2px;\n" +
                "  border-color: #000000;\n" +
                "  border-style: solid;\n" +
                "  color: #000000;\n" +
                "}\n" +
                "\n" +
                "table.GeneratedTable td, table.GeneratedTable th {\n" +
                "  border-width: 2px;\n" +
                "  border-color: #000000;\n" +
                "  border-style: solid;\n" +
                "  padding: 5px;\n" +
                "}\n" +
                "\n" +
                "table.GeneratedTable thead {\n" +
                "  background-color: #ffffff;\n" +
                "}\n" +
                "</style>\n" +
                "\n" +
                "<table class=\"GeneratedTable\">\n" +
                "  <thead>\n" +
                "    <tr>\n" +
                "      <th width=\"10%\">CODE</th>\n" +
                "      <th width=\"20%\">STATUS</th>\n" +
                "      <th width=\"70%\">JSON RESPONSE</th>\n" +
                "    </tr>\n" +
                "  </thead>\n" +
                "  <tbody>\n" +
                "    <tr>\n" +
                "      <td>200</td>\n" +
                "      <td>OK - list results</td>\n" +
                "      <td><pre>\n" +
                "[\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"CUENTA CORRIENTE\",\n" +
                "    \"currency\": \"ARG\",\n" +
                "    \"control_code\": \"01\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 2,\n" +
                "    \"name\": \"CUENTA CORRIENTE\",\n" +
                "    \"currency\": \"USD\",\n" +
                "    \"control_code\": \"02\"\n" +
                "  }\n" +
                "]\n" +
                "      </pre></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td>204</td>\n" +
                "      <td>No Content - empty array</td>\n" +
                "      <td><pre>\n" +
                "[]\n" +
                "      </pre></td>\n" +
                "    </tr>\n" +
                "  </tbody>\n" +
                "</table>\n";

        String valueDelete = "Delete Account Type by ID";

        String noteDelete = "# Delete Account Type by ID\n" +
                "\n" +
                "The response with status 200 state (OK) with empty body or 409 If an error happens with the description of the error in the body.\n<br>" +
                "\n" +
                "<style>\n" +
                "table.GeneratedTable {\n" +
                "  width: 100%;\n" +
                "  background-color: #ffffff;\n" +
                "border-collapse: separate;\n" +
                "  border-width: 2px;\n" +
                "  border-color: #000000;\n" +
                "  border-style: solid;\n" +
                "  color: #000000;\n" +
                "}\n" +
                "\n" +
                "table.GeneratedTable td, table.GeneratedTable th {\n" +
                "  border-width: 2px;\n" +
                "  border-color: #000000;\n" +
                "  border-style: solid;\n" +
                "  padding: 5px;\n" +
                "}\n" +
                "\n" +
                "table.GeneratedTable thead {\n" +
                "  background-color: #ffffff;\n" +
                "}\n" +
                "</style>\n" +
                "\n" +
                "<table class=\"GeneratedTable\">\n" +
                "  <thead>\n" +
                "    <tr>\n" +
                "      <th width=\"10%\">CODE</th>\n" +
                "      <th width=\"20%\">STATUS</th>\n" +
                "      <th width=\"70%\">JSON RESPONSE</th>\n" +
                "    </tr>\n" +
                "  </thead>\n" +
                "  <tbody>\n" +
                "    <tr>\n" +
                "      <td>200</td>\n" +
                "      <td>OK - empty body</td>\n" +
                "      <td><pre></pre></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td>409</td>\n" +
                "      <td>Conflict - record not found to delete</td>\n" +
                "      <td><pre>\n" +
                "{\n" +
                "  \"status\": \"CONFLICT\",\n" +
                "  \"errors\": {\n" +
                "    \"id\": {\n" +
                "      \"cant_delete_record_not_found\": \"can't delete the registry, non-existent record in the database\"\n" +
                "    }\n" +
                "  }\n" +
                "}\n" +
                "      </pre></td>\n" +
                "    </tr>\n" +
                "  </tbody>\n" +
                "</table>\n";
    }
}
