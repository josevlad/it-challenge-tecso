package es.coop.tecso.bank.controller;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.BankAccountCreateDTO;
import es.coop.tecso.bank.model.dto.BankAccountDTO;
import es.coop.tecso.bank.service.BankAccountService;
import es.coop.tecso.bank.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
public class BankAccountController extends MainRestController {

    private final Logger log = LoggerFactory.getLogger(BankAccountController.class);

    private static final String ENTITY_NAME = "baseBankAccount";

    @Autowired
    private BankAccountService bankAccountService;

    /**
     * POST  /bank-accounts : Create a new bankAccount.
     *
     * @param bankAccountDTO the bankAccountDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bankAccountDTO, or with status 400 (Bad Request) if the bankAccount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bank-accounts")
    public ResponseEntity<BankAccountDTO> createBankAccount(
            @Valid @RequestBody BankAccountCreateDTO bankAccountDTO,
            BindingResult errors) throws URISyntaxException, StandardApplicationException {

        log.debug("REST request to save BankAccount : {}", bankAccountDTO);

        if (errors.hasErrors())
            throw new StandardApplicationException(HttpStatus.BAD_REQUEST, errors);

        BankAccountDTO result = bankAccountService.save(bankAccountDTO);
        return ResponseEntity.created(new URI("/api/bank-accounts/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * GET  /customers/:id/bank-accounts : get all the bankAccounts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of bankAccounts in body
     */
    @GetMapping("/customers/{id}/bank-accounts")
    public List<BankAccountDTO> getAllBankAccounts(@PathVariable Long id) {
        log.debug("REST request to get all BankAccounts");
        return bankAccountService.findAllByCustomerId(id);
    }

    /**
     * DELETE  /bank-accounts/:id : delete the "id" bankAccount.
     *
     * @param id the id of the bankAccountDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/bank-accounts/{id}")
    public ResponseEntity<Void> deleteBankAccount(@PathVariable Long id) throws StandardApplicationException {
        log.debug("REST request to delete BankAccount : {}", id);
        bankAccountService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
