package es.coop.tecso.bank.controller;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.CountryDTO;
import es.coop.tecso.bank.service.CountryService;
import es.coop.tecso.bank.util.ControllerUtil;
import es.coop.tecso.bank.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


@RestController
public class CountryController extends MainRestController {

    private final Logger log = LoggerFactory.getLogger(CountryController.class);

    private static final String ENTITY_COUNTRY = "tecsoBankCountry";

    @Autowired
    private CountryService countryService;

    /**
     * POST  /countries : Create a new country.
     *
     * @param countryDTO the countryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new countryDTO, or with status 400 (Bad Request) if the country has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/countries")
    public ResponseEntity<CountryDTO> createCountry(
            @Valid @RequestBody CountryDTO countryDTO,
            BindingResult errors) throws URISyntaxException, StandardApplicationException {
        log.debug("REST request to save Country : {}", countryDTO);
        if (countryDTO.getId() != null)
            errors.addError(new FieldError(
                    "countryDTO",
                    ENTITY_COUNTRY + ".id",
                    "A new country cannot already have an ID"));

        if (errors.hasErrors()) throw new StandardApplicationException(HttpStatus.BAD_REQUEST, errors);

        CountryDTO result = countryService.save(countryDTO);
        return ResponseEntity.created(new URI("/api/v1/countries/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_COUNTRY, result.getId().toString()))
                .body(result);
    }

    /**
     * GET  /countries : get all the countries.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of countries in body
     */
    @GetMapping("/countries")
    public ResponseEntity<?> getAllCountries() {
        log.debug("REST request to get all Countries");
        List<CountryDTO> all = countryService.findAll();
        HttpStatus status = (all.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return ControllerUtil
                .buildResponser(status, HeaderUtil.createAlert(ENTITY_COUNTRY, "all-countries"), all);
    }

    /**
     * DELETE  /countries/:id : delete the "id" country.
     *
     * @param id the id of the countryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/countries/{id}")
    public ResponseEntity<Void> deleteCountry(@PathVariable Long id) throws StandardApplicationException {
        log.debug("REST request to delete Country : {}", id);
        countryService.delete(id);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_COUNTRY, id.toString()))
                .build();
    }
}
