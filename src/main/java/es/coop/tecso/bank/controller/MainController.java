package es.coop.tecso.bank.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class MainController {

    /**
     * @return
     */
    @GetMapping("/apidoc")
    public ModelAndView redirectToSwaggerUI() {
        return new ModelAndView("redirect:/swagger-ui.html");
    }
}
