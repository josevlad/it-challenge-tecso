package es.coop.tecso.bank.controller;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.BankDTO;
import es.coop.tecso.bank.service.BankService;
import es.coop.tecso.bank.util.ControllerUtil;
import es.coop.tecso.bank.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


@RestController
public class BankController extends MainRestController {

    private final Logger log = LoggerFactory.getLogger(BankController.class);

    private static final String ENTITY_BANK = "tecsoBankBank";

    @Autowired
    private BankService bankService;

    /**
     * POST  /banks : Create a new bank.
     *
     * @param bankDTO the bankDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bankDTO, or with status 400 (Bad Request) if the bank has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/banks")
    public ResponseEntity<BankDTO> createBank(
            @Valid @RequestBody BankDTO bankDTO,
            BindingResult errors) throws URISyntaxException, StandardApplicationException {

        log.debug("REST request to save Bank : {}", bankDTO);
        if (bankDTO.getId() != null)
            errors.addError(new FieldError(
                    "bankDTO",
                    ENTITY_BANK + ".id",
                    "A new country cannot already have an ID"));

        if (errors.hasErrors()) throw new StandardApplicationException(HttpStatus.BAD_REQUEST, errors);

        BankDTO result = bankService.save(bankDTO);
        return ResponseEntity.created(new URI("/api/v1/banks/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_BANK, result.getId().toString()))
                .body(result);
    }

    /**
     * GET  /banks : get all the banks.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of banks in body
     */
    @GetMapping("/banks")
    public ResponseEntity<?> getAllBanks() {
        log.debug("REST request to get all Banks");
        List<BankDTO> all = bankService.findAll();
        HttpStatus status = (all.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return ControllerUtil
                .buildResponser(status, HeaderUtil.createAlert(ENTITY_BANK, "all-banks"), all);
    }

    /**
     * DELETE  /banks/:id : delete the "id" bank.
     *
     * @param id the id of the bankDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/banks/{id}")
    public ResponseEntity<Void> deleteBank(@PathVariable Long id) {
        log.debug("REST request to delete Bank : {}", id);
        bankService.delete(id);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_BANK, id.toString()))
                .build();
    }
}
