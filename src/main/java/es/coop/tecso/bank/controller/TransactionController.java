package es.coop.tecso.bank.controller;

import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.AddTransactionDTO;
import es.coop.tecso.bank.model.dto.TransactionDTO;
import es.coop.tecso.bank.service.TransactionService;
import es.coop.tecso.bank.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
public class TransactionController extends MainRestController {

    private final Logger log = LoggerFactory.getLogger(TransactionController.class);

    private static final String ENTITY_NAME = "baseTransaction";

    @Autowired
    private TransactionService transactionService;

    /**
     * POST  /bank-accounts/:id/transactions : Create a new transaction.
     *
     * @param transactionDTO the transactionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new transactionDTO, or with status 400 (Bad Request) if the transaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bank-accounts/{id}/transactions")
    public ResponseEntity<TransactionDTO> createTransaction(
            @PathVariable Long id,
            @Valid @RequestBody AddTransactionDTO transactionDTO,
            BindingResult errors) throws URISyntaxException, StandardApplicationException {
        log.debug("REST request to save Transaction : {}", transactionDTO);

        if (errors.hasErrors())
            throw new StandardApplicationException(HttpStatus.BAD_REQUEST, errors);

        TransactionDTO result = transactionService.save(transactionDTO.generateTransactionDTO(), id);
        return ResponseEntity.created(new URI("/bank-accounts/" + result.getId() + "/transactions"))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * GET  /bank-accounts/:id/transactions : get all the transactions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of transactions in body
     */
    @GetMapping("/bank-accounts/{id}/transactions")
    public List<TransactionDTO> getAllTransactions(@PathVariable Long id) {
        log.debug("REST request to get all Transactions");
        return transactionService.findAllByBankAccountId(id);
    }
}
