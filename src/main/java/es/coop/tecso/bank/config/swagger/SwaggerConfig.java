package es.coop.tecso.bank.config.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static com.google.common.collect.Lists.newArrayList;
import static org.springframework.http.HttpStatus.*;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Bean
    public Docket apiDocket() {
        // Class[] clazz = { AccountTypeError.class };
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage("es.coop.tecso.bank.controller"))
                .paths(PathSelectors.any())
                .build()
                .enable(true)
                //.ignoredParameterTypes(clazz)
                .apiInfo(getApiInfo());

        //setTags(docket);
        //setGlobalsResponseMessage(docket);

        return docket;
    }

    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "IT Challenge Tecso",
                "Project for Spring Boot - IT Challenge Tecso",
                "0.0.1",
                null,
                new Contact("Vladimir Castañeda", "URL", "vladimir29castaneda@gmail.com"),
                null,
                null,
                Collections.emptyList()
        );
    }

    private void setTags(Docket docket) {
        docket.tags(new Tag(
                "Persons Service",
                "Servicio para las personas",
                1)
        );
    }

    private void setGlobalsResponseMessage(Docket docket) {
        docket.globalResponseMessage(RequestMethod.GET,
                newArrayList(
                        new ResponseMessageBuilder().code(BAD_REQUEST.value()).message(BAD_REQUEST.toString()).build(),
                        new ResponseMessageBuilder().code(FORBIDDEN.value()).message(FORBIDDEN.toString()).build(),
                        new ResponseMessageBuilder().code(NOT_FOUND.value()).message(NOT_FOUND.toString()).build(),
                        new ResponseMessageBuilder().code(CONFLICT.value()).message(CONFLICT.toString()).build(),
                        new ResponseMessageBuilder().code(INTERNAL_SERVER_ERROR.value()).message(INTERNAL_SERVER_ERROR.toString()).build()
                )
        );
    }
}
