package es.coop.tecso.bank.model.mapper;

import es.coop.tecso.bank.model.dto.AccountTypeDTO;
import es.coop.tecso.bank.model.entity.AccountType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface AccountTypeMapper extends EntityMapper<AccountTypeDTO, AccountType> {

    @Mapping(target = "bankAccounts", ignore = true)
    AccountType toEntity(AccountTypeDTO accountTypeDTO);

    default AccountType fromId(Long id) {
        if (id == null) {
            return null;
        }
        AccountType accountType = new AccountType();
        accountType.setId(id);
        return accountType;
    }
}
