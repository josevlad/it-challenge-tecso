package es.coop.tecso.bank.model.mapper;

import es.coop.tecso.bank.model.dto.TransactionDTO;
import es.coop.tecso.bank.model.entity.Transaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = { TransactionTypeMapper.class, BankAccountMapper.class })
public interface TransactionMapper extends EntityMapper<TransactionDTO, Transaction> {

    @Mapping(target = "transactionTypeId", ignore = true)
    TransactionDTO toDto(Transaction transaction);

    Transaction toEntity(TransactionDTO transactionDTO);

    default Transaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        Transaction transaction = new Transaction();
        transaction.setId(id);
        return transaction;
    }
}
