package es.coop.tecso.bank.model.mapper;

import es.coop.tecso.bank.model.dto.BankDTO;
import es.coop.tecso.bank.model.entity.Bank;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = { CountryMapper.class })
public interface BankMapper extends EntityMapper<BankDTO, Bank> {

    @Mapping(target = "countryCode", ignore = true)
    BankDTO toDto(Bank bank);

    @Mapping(target = "agencies", ignore = true)
    Bank toEntity(BankDTO bankDTO);

    default Bank fromId(Long id) {
        if (id == null) {
            return null;
        }
        Bank bank = new Bank();
        bank.setId(id);
        return bank;
    }
}
