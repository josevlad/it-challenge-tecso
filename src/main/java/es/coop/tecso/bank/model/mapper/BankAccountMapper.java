package es.coop.tecso.bank.model.mapper;

import es.coop.tecso.bank.model.dto.BankAccountDTO;
import es.coop.tecso.bank.model.entity.BankAccount;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = { AgencyMapper.class, AccountTypeMapper.class, CustomerMapper.class })
public interface BankAccountMapper extends EntityMapper<BankAccountDTO, BankAccount> {

    BankAccountDTO toDto(BankAccount bankAccount);

    @Mapping(target = "transactions", ignore = true)
    @Mapping(target = "customer", ignore = true)
    @Mapping(target = "updateBalanceCredit", ignore = true)
    @Mapping(target = "updateBalanceDebit", ignore = true)
    BankAccount toEntity(BankAccountDTO bankAccountDTO);

    default BankAccount fromId(Long id) {
        if (id == null) {
            return null;
        }
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(id);
        return bankAccount;
    }
}
