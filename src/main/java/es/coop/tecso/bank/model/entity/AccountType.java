package es.coop.tecso.bank.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "account_type")
public class AccountType implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "currency")
    private String currency;

    @Column(name = "control_code")
    private String controlCode;

    @OneToMany(mappedBy = "accountType")
    private Set<BankAccount> bankAccounts = new HashSet<>();

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getControlCode() {
        return controlCode;
    }

    public void setControlCode(String controlCode) {
        this.controlCode = controlCode;
    }

    public Set<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    /****************************************************************************************************/

    public AccountType name(String name) {
        this.name = name;
        return this;
    }

    public AccountType currency(String currency) {
        this.currency = currency;
        return this;
    }

    public AccountType controlCode(String controlCode) {
        this.controlCode = controlCode;
        return this;
    }

    public AccountType bankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
        return this;
    }

    public AccountType addBankAccount(BankAccount bankAccount) {
        this.bankAccounts.add(bankAccount);
        bankAccount.setAccountType(this);
        return this;
    }

//    public AccountType removeBankAccount(BankAccount bankAccount) {
//        this.bankAccounts.remove(bankAccount);
//        bankAccount.setAccountTypeId(null);
//        return this;
//    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountType accountType = (AccountType) o;
        if (accountType.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accountType.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AccountType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", currency='" + getCurrency() + "'" +
            ", controlCode='" + getControlCode() + "'" +
            "}";
    }
}
