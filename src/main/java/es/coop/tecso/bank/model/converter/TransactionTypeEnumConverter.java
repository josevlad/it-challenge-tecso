package es.coop.tecso.bank.model.converter;

import es.coop.tecso.bank.model.dto.enums.TransactionTypeEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TransactionTypeEnumConverter implements AttributeConverter<TransactionTypeEnum, String> {
    @Override
    public String convertToDatabaseColumn(TransactionTypeEnum attribute) {
        return attribute.getTransactionType();
    }

    @Override
    public TransactionTypeEnum convertToEntityAttribute(String dbData) {
        return TransactionTypeEnum.getFromAttributeValue(dbData);
    }
}
