package es.coop.tecso.bank.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;
import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.RegularExpressions.REGEXP_ONLY_LETTERS;
import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.RegularExpressions.REGEXP_ONLY_LETTERS_WITH_SPACE;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "country_code" })
public class CountryDTO implements Serializable {

    private Long id;

    @NotBlank(message = NOT_BLANK)
    @Pattern(regexp = REGEXP_ONLY_LETTERS_WITH_SPACE, message = ONLY_LETTERS)
    @JsonProperty("name")
    private String name;

    @NotBlank(message = NOT_BLANK)
    @Pattern(regexp = REGEXP_ONLY_LETTERS, message = ONLY_LETTERS)
    @Size(max = 2, message = MAX_SIZE + " [max = 2]")
    @JsonProperty("country_code")
    private String countryCode;

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.toUpperCase().trim();
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode.toUpperCase().trim();
    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CountryDTO countryDTO = (CountryDTO) o;
        if (countryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), countryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CountryDTO{" +
                "id=" + getId() +
                ", name='" + getName() + "'" +
                ", countryCode='" + getCountryCode() + "'" +
                "}";
    }
}
