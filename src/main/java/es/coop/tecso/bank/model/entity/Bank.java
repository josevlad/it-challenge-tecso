package es.coop.tecso.bank.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "bank")
public class Bank implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Size(max = 4)
    @Column(name = "iba_code", length = 4)
    private String ibaCode;

    @Size(max = 4)
    @Column(name = "bank_code", length = 4)
    private String bankCode;

    @ManyToOne
    @JsonIgnoreProperties("banks")
    private Country country;

    @OneToMany(mappedBy = "bank")
    private Set<Agency> agencies = new HashSet<>();

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIbaCode() {
        return ibaCode;
    }

    public void setIbaCode(String ibaCode) {
        this.ibaCode = ibaCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Set<Agency> getAgencies() {
        return agencies;
    }

    public void setAgencies(Set<Agency> agencies) {
        this.agencies = agencies;
    }

    /****************************************************************************************************/

    public Bank name(String name) {
        this.name = name;
        return this;
    }

    public Bank ibaCode(String ibaCode) {
        this.ibaCode = ibaCode;
        return this;
    }

    public Bank bankCode(String bankCode) {
        this.bankCode = bankCode;
        return this;
    }

    public Bank country(Country country) {
        this.country = country;
        return this;
    }

    public Bank agencies(Set<Agency> agencies) {
        this.agencies = agencies;
        return this;
    }

    public Bank addAgency(Agency agency) {
        this.agencies.add(agency);
        agency.setBank(this);
        return this;
    }

//    public Bank removeAgency(Agency agency) {
//        this.agencies.remove(agency);
//        agency.setBank(null);
//        return this;
//    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bank bank = (Bank) o;
        if (bank.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bank.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Bank{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", ibaCode='" + getIbaCode() + "'" +
            ", bankCode='" + getBankCode() + "'" +
            "}";
    }
}
