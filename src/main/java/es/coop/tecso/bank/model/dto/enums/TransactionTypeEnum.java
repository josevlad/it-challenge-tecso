package es.coop.tecso.bank.model.dto.enums;

import com.google.common.collect.Maps;

import java.util.EnumSet;
import java.util.Map;

public enum TransactionTypeEnum {
    CREDIT("CRÉDITO"),
    DEBIT("DÉBITO");

    private final String transactionType;

    private static Map<String, TransactionTypeEnum> transactionTypeEnumMap =
            Maps.uniqueIndex(EnumSet.allOf(TransactionTypeEnum.class), TransactionTypeEnum::getTransactionType);

    TransactionTypeEnum(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public static TransactionTypeEnum getFromAttributeValue(String attributeValue) {
        return transactionTypeEnumMap.get(attributeValue);
    }

    @Override
    public String toString() {
        return "TransactionTypeEnum {" +
                "transactionType='" + transactionType + '\'' +
                '}';
    }
}
