package es.coop.tecso.bank.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import es.coop.tecso.bank.component.validator.enumvalidator.EnumValue;
import es.coop.tecso.bank.model.dto.enums.DocumentTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Objects;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;
import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.RegularExpressions.REGEXP_ONLY_LETTERS_WITH_SPACE;
import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.RegularExpressions.REGEXP_ONLY_NUMBERS;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Data model for updating customer data. all properties are optional, only one of them should be sent")
@JsonPropertyOrder({ "id", "name", "last_name", "document_type", "document_number" })
public class CustomerUpdateDTO implements Serializable {

    @ApiModelProperty(notes = ApiDoc.idNote, example = ApiDoc.idExample, position = 0, readOnly = true)
    private Long id;

    @JsonProperty("name")
    @Pattern(regexp = REGEXP_ONLY_LETTERS_WITH_SPACE, message = ONLY_LETTERS)
    @ApiModelProperty(notes = ApiDoc.nameNote, example = ApiDoc.nameExample, position = 1)
    private String name;

    @JsonProperty("last_name")
    @Pattern(regexp = REGEXP_ONLY_LETTERS_WITH_SPACE, message = ONLY_LETTERS)
    @ApiModelProperty(notes = ApiDoc.lastNameNote, example = ApiDoc.lastNameExample, position = 2)
    private String lastName;

    @JsonProperty("document_number")
    @Pattern(regexp = REGEXP_ONLY_NUMBERS, message = ONLY_NUMBERS)
    @ApiModelProperty(notes = ApiDoc.dnNote, example = ApiDoc.dnExample, position = 3)
    private String documentNumber;

    @JsonProperty("document_type")
    @EnumValue(enumClass = DocumentTypeEnum.class, message = ENUM_VALUES)
    @ApiModelProperty(notes = ApiDoc.dtNote, example = ApiDoc.dtExample, allowableValues = ApiDoc.dtAllowableValues, position = 4)
    private String documentType;

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomerUpdateDTO customerDTO = (CustomerUpdateDTO) o;
        if (customerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), customerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
                "id=" + getId() +
                ", name='" + getName() + "'" +
                ", lastName='" + getLastName() + "'" +
                ", documentNumber='" + getDocumentNumber() + "'" +
                ", documentType='" + getDocumentType() + "'" +
                "}";
    }

    private interface ApiDoc {
        String idNote = "Represents the primary key<br>";
        String idExample = "1";

        String nameNote = "Represent the customer's name.<br>";
        String nameExample = "JOSE GREGORIO";

        String lastNameNote = "Represent the customer's last name.<br>";
        String lastNameExample = "PEREZ JIMENEZ";

        String dnNote = "Represent the customer's document number ID.";
        String dnExample = "26590441";

        String dtNote = "only one of the allowed values is accepted.";
        String dtAllowableValues = "DNI, LE, LC, PASSPORT";
        String dtExample = "DNI";
    }
}
