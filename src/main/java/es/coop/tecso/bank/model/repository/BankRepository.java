package es.coop.tecso.bank.model.repository;

import es.coop.tecso.bank.model.entity.Bank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@SuppressWarnings("unused")
@Repository
public interface BankRepository extends JpaRepository<Bank, Long> {

    /**
     * @param bankCode
     * @return
     */
    Optional<Bank> findByBankCode(String bankCode);

    /**
     * @return
     */
    Optional<Bank> findFirstByOrderByIdDesc();

    /**
     * @param ibaCode
     * @return
     */
    @Query(value = "SELECT b.* FROM bank AS b WHERE b.iba_code LIKE ?1% ORDER BY b.id DESC LIMIT 1", nativeQuery = true)
    Optional<Bank> findLastInsertByCountryCode(String ibaCode);

    /**
     * @return
     */
    @Query(value = "SELECT b.* FROM bank AS b ORDER BY b.id DESC LIMIT 1", nativeQuery = true)
    Optional<Bank> findLastInsert();


}
