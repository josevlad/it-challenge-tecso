package es.coop.tecso.bank.model.repository;

import es.coop.tecso.bank.model.entity.Agency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("unused")
@Repository
public interface AgencyRepository extends JpaRepository<Agency, Long> {

    /**
     * @return
     */
    @Query(value = "SELECT a.* FROM agency AS a ORDER BY a.id DESC LIMIT 1", nativeQuery = true)
    Optional<Agency> findLastInsert();

    /**
     * @param id
     * @return
     */
    List<Agency> findAllByBankId(Long id);

    /**
     * @param agencyCode
     * @return
     */
    Optional<Agency> findByAgencyCode(String agencyCode);
}
