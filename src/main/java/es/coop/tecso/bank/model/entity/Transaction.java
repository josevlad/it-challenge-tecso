package es.coop.tecso.bank.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.enums.TransactionTypeEnum;
import es.coop.tecso.bank.util.ServicesImplUtil;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Objects;

@Entity
@Table(name = "transaction")
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreationTimestamp
    @Column(name = "transaction_date")
    private Timestamp transactionDate;

    @Size(max = 200)
    @Column(name = "description", length = 200)
    private String description;

    @Transient
    private String amount;

    @Column(name = "amount", precision = 10, scale = 2)
    private BigDecimal amountDB;

    @ManyToOne
    @JsonIgnoreProperties("transactions")
    private TransactionType transactionType;

    @ManyToOne
    @JsonIgnoreProperties("transactions")
    private BankAccount bankAccount;

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Timestamp transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public BigDecimal getAmountDB() {
        return amountDB;
    }

    public void setAmountDB(BigDecimal amountDB) {
        this.amountDB = amountDB;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    /****************************************************************************************************/

    public Transaction transactionDate(Timestamp transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    public Transaction description(String description) {
        this.description = description;
        return this;
    }

    public Transaction amount(BigDecimal amount) {
        this.amountDB = amount;
        return this;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public Transaction transactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
        return this;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public Transaction bankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
        return this;
    }

    public Transaction formatAmount() {
        Locale l = ServicesImplUtil.getLocaleByCurrency(bankAccount.getAccountType().getCurrency());
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(l);
        amountDB = new BigDecimal(amount);
        amount = numberFormat.format(amountDB);
        return this;
    }

    public Transaction updateBalancebankAccount() throws StandardApplicationException {
        if (this.transactionType.getName().equals(TransactionTypeEnum.CREDIT))
            bankAccount = bankAccount.updateBalanceCredit(amountDB);

        if (this.transactionType.getName().equals(TransactionTypeEnum.DEBIT))
            bankAccount = bankAccount.updateBalanceDebit(amountDB);

        return this;
    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Transaction transaction = (Transaction) o;
        if (transaction.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), transaction.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Transaction{" +
            "id=" + getId() +
            ", transactionDate='" + getTransactionDate() + "'" +
            ", description='" + getDescription() + "'" +
            ", amountDB=" + getAmountDB() +
            "}";
    }
}
