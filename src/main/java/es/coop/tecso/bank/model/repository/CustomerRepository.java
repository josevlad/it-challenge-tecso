package es.coop.tecso.bank.model.repository;

import es.coop.tecso.bank.model.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@SuppressWarnings("unused")
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    /**
     * @param documentNumber
     * @return
     */
    Optional<Customer> findByDocumentNumber(String documentNumber);
}
