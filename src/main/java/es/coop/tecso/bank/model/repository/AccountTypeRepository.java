package es.coop.tecso.bank.model.repository;

import es.coop.tecso.bank.model.entity.AccountType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("unused")
@Repository
public interface AccountTypeRepository extends JpaRepository<AccountType, Long> {

    /**
     * @param controlCode
     * @return
     */
    List<AccountType> findByCurrency(String controlCode);

    /**
     * @return
     */
    @Query(value = "SELECT at.* FROM account_type AS at ORDER BY at.id DESC LIMIT 1", nativeQuery = true)
    Optional<AccountType> findLastInsert();

    /**
     * @param name
     * @return
     */
    Optional<AccountType> findByName(String name);
}
