package es.coop.tecso.bank.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import es.coop.tecso.bank.component.validator.enumvalidator.EnumValue;
import es.coop.tecso.bank.model.dto.enums.CurrencyEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Objects;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;
import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.RegularExpressions.REGEXP_ONLY_LETTERS_WITH_SPACE;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "currency", "control_code" })
@ApiModel(description = "Class representing a a for Bank account.")
public class AccountTypeDTO implements Serializable {

    @ApiModelProperty(notes = ApiDoc.idNote, example = ApiDoc.idExample, position = 0, readOnly = true)
    private Long id;


    @JsonProperty("name")
    @NotBlank(message = NOT_BLANK)
    @Pattern(regexp = REGEXP_ONLY_LETTERS_WITH_SPACE, message = ONLY_LETTERS)
    @ApiModelProperty(notes = ApiDoc.nameNote, example = ApiDoc.nameExample, required = true, position = 1)
    private String name;

    @JsonProperty("currency")
    @NotBlank(message = NOT_BLANK)
    @EnumValue(enumClass = CurrencyEnum.class, message = ENUM_VALUES)
    @ApiModelProperty(notes = ApiDoc.currencyNote, allowableValues = ApiDoc.allowableValues, example = ApiDoc.currencyExample, required = true, position = 2)
    private String currency;

    @JsonProperty("control_code")
    @ApiModelProperty(notes = ApiDoc.controlCodeNote, example = ApiDoc.controlCodeExample, position = 3, readOnly = true)
    private String controlCode;

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.toUpperCase().trim();
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency.toUpperCase().trim();
    }

    public String getControlCode() {
        return controlCode;
    }

    public void setControlCode(String controlCode) {
        this.controlCode = controlCode;
    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountTypeDTO accountTypeDTO = (AccountTypeDTO) o;
        if (accountTypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accountTypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AccountTypeDTO{" +
                "id=" + getId() +
                ", name='" + getName() + "'" +
                ", currency='" + getCurrency() + "'" +
                ", controlCode='" + getControlCode() + "'" +
                "}";
    }

    private interface ApiDoc {
        String idNote = "Represents the primary key<br>";
        String idExample = "1";

        String nameNote = "In combination with the currency, this is the only possible value.<br>";
        String nameExample = "CUENTA DE AHORROS";

        String currencyNote = "In combination with the name, this is the only possible value.";
        String allowableValues = "ARG,USD,EUR";
        String currencyExample = "USD";

        String controlCodeNote = "It is a unique number that is part of the bank account number. It is generated automatically in the system.";
        String controlCodeExample = "83";
    }
}
