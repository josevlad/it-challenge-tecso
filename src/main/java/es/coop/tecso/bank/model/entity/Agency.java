package es.coop.tecso.bank.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "agency")
public class Agency implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name")
    private String name;

    @Unique
    @NotNull
    @Column(name = "agency_code")
    private String agencyCode;

    @ManyToOne
    @JsonIgnoreProperties("agencies")
    private Bank bank;

    @OneToMany(mappedBy = "agency")
    private Set<BankAccount> bankAccounts = new HashSet<>();

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Set<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    /****************************************************************************************************/

    public Agency name(String name) {
        this.name = name;
        return this;
    }

    public Agency agencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
        return this;
    }

    public Agency bank(Bank bank) {
        this.bank = bank;
        return this;
    }

    public Agency bankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
        return this;
    }

    public Agency addBankAccount(BankAccount bankAccount) {
        this.bankAccounts.add(bankAccount);
        bankAccount.setAgency(this);
        return this;
    }

//    public Agency removeBankAccount(BankAccount bankAccount) {
//        this.bankAccounts.remove(bankAccount);
//        bankAccount.setAgency(null);
//        return this;
//    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Agency agency = (Agency) o;
        if (agency.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), agency.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Agency{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", agencyCode='" + getAgencyCode() + "'" +
            "}";
    }
}
