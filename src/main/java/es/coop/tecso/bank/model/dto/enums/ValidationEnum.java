package es.coop.tecso.bank.model.dto.enums;

import com.google.common.collect.Maps;

import java.util.EnumSet;
import java.util.Map;

public enum ValidationEnum {

    NOT_NULL(Messages.NOT_NULL),
    NOT_EMPTY(Messages.NOT_BLANK),
    ONLY_LETTERS(Messages.ONLY_LETTERS),
    ONLY_NUMBERS(Messages.ONLY_NUMBERS),
    ALPHANUMERIC(Messages.ALPHANUMERIC),
    MAX_SIZE(Messages.MAX_SIZE),
    MIN_SIZE(Messages.MIN_SIZE),
    ENUM_VALUES(Messages.ENUM_VALUES),
    OBJECT_NULL(Messages.OBJECT_NULL),
    TWO_DECIMAL(Messages.TWO_DECIMAL),

    CANT_SAVE_OR_UPDATE_ASSIGNED_CODE(Messages.CANT_SAVE_OR_UPDATE_ASSIGNED_CODE),
    CANT_SAVE_OR_UPDATE_ASSIGNED_NAME(Messages.CANT_SAVE_OR_UPDATE_ASSIGNED_NAME),
    CANT_SAVE_OR_UPDATE_ASSIGNED_ID(Messages.CANT_SAVE_OR_UPDATE_ASSIGNED_ID),
    CANT_SAVE_OR_UPDATE_ALREADY_EXISTS(Messages.CANT_SAVE_OR_UPDATE_ALREADY_EXISTS),
    CANT_SAVE_OR_UPDATE_RECORD_NOT_FOUND(Messages.CANT_SAVE_OR_UPDATE_RECORD_NOT_FOUND),
    CANT_DELETE_RECORD_NOT_FOUND(Messages.CANT_DELETE_RECORD_NOT_FOUND),
    CANT_DELETE_DEPENDENT_ENTITY(Messages.CANT_DELETE_DEPENDENT_ENTITY),

    INSUFFICIENT_BALANCE(Messages.INSUFFICIENT_BALANCE),
    LIMIT_EXCEEDED_ARG(Messages.LIMIT_EXCEEDED_ARG),
    LIMIT_EXCEEDED_USD(Messages.LIMIT_EXCEEDED_USD),
    LIMIT_EXCEEDED_EUR(Messages.LIMIT_EXCEEDED_EUR)
    ;

    private String messages;

    private static Map<String, ValidationEnum> validationMessagesMap =
            Maps.uniqueIndex(EnumSet.allOf(ValidationEnum.class), ValidationEnum::getMessages);


    ValidationEnum(String messages) {
        this.messages = messages;
    }

    public String getMessages() {
        return messages;
    }

    public static ValidationEnum getFromMessages(String messages) {
        return validationMessagesMap.get(messages);
    }

    public static class Messages {

        public static final String SEE_API_DOC = ", see RESTful API Documentation in Models section";

        // DATA INTEGRITY
        public static final String NOT_NULL = "this field can't be null or empty" + SEE_API_DOC;
        public static final String NOT_BLANK = "this field is required" + SEE_API_DOC;
        public static final String ONLY_LETTERS = "valid only letters characters" + SEE_API_DOC;
        public static final String ONLY_NUMBERS = "valid only numeric characters" + SEE_API_DOC;
        public static final String ALPHANUMERIC = "valid only alphanumeric characters" + SEE_API_DOC;
        public static final String MAX_SIZE = "exceeded character limit" + SEE_API_DOC;
        public static final String MIN_SIZE = "insufficient minimum character limit" + SEE_API_DOC;
        public static final String ENUM_VALUES = "this value not is one of the declared names" + SEE_API_DOC;
        public static final String OBJECT_NULL = "At least one field should not be empty or null" + SEE_API_DOC;
        public static final String TWO_DECIMAL = "the amount must be expressed as an integer or with 2 decimals" + SEE_API_DOC;

        // VALIDATION IN DATABASE
        public static final String RECORD_NOT_FOUND = "non-existent record in the database";

        public static final String ASSIGNED_CODE = "there is another with the same code";
        public static final String ASSIGNED_NAME = "there is another with the same name";
        public static final String ASSIGNED_ID = "there is another with the same number id";
        public static final String ALREADY_EXISTS = "this already exists in the database";
        public static final String DEPENDENT_ENTITY = "the entity has other associated records";


        public static final String CANT_SAVE_OR_UPDATE_ASSIGNED_CODE = "can't save or update the registry, "+ ASSIGNED_CODE;
        public static final String CANT_SAVE_OR_UPDATE_ASSIGNED_NAME = "can't save or update the registry, " + ASSIGNED_NAME;
        public static final String CANT_SAVE_OR_UPDATE_ASSIGNED_ID = "can't save or update the registry, " + ASSIGNED_ID;
        public static final String CANT_SAVE_OR_UPDATE_ALREADY_EXISTS = "can't save or update the registry, "+ ALREADY_EXISTS;
        public static final String CANT_SAVE_OR_UPDATE_RECORD_NOT_FOUND = "can't save or update the registry, " + RECORD_NOT_FOUND;
        public static final String CANT_DELETE_RECORD_NOT_FOUND = "can't delete the registry, " + RECORD_NOT_FOUND;
        public static final String CANT_DELETE_DEPENDENT_ENTITY = "can't delete the registry, " + DEPENDENT_ENTITY;

        public static final String INSUFFICIENT_BALANCE = "You do not have enough funds to make the debit";
        public static final String LIMIT_EXCEEDED_ARG = "Subtraction limit exceeded. You can debit until 1000";
        public static final String LIMIT_EXCEEDED_USD = "Subtraction limit exceeded. You can debit until 300";
        public static final String LIMIT_EXCEEDED_EUR = "Subtraction limit exceeded. You can debit until 150";

    }

    public static class RegularExpressions {
        public static final String REGEXP_ONLY_LETTERS_WITH_SPACE = "^[a-zA-ZáéíóúÁÉÍÓÚÜüñÑ\\s]*$";
        public static final String REGEXP_ONLY_LETTERS = "^[a-zA-ZáéíóúÁÉÍÓÚÜüñÑ]*$";
        public static final String REGEXP_ONLY_NUMBERS = "[0-9]+";
        public static final String REGEXP_ALPHANUMERIC = "^[0-9a-zA-ZáéíóúÁÉÍÓÚÜüñÑ]*$";
        public static final String REGEXP_ALPHANUMERIC_WITH_SPACE = "^[0-9a-zA-ZáéíóúÁÉÍÓÚÜüñÑ\\s]*$";
        public static final String REGEXP_TWO_DECIMAL = "^\\d+(\\.\\d{1,2})?$";
    }

    @Override
    public String toString() {
        return "ValidationEnum{" +
                "messages='" + messages + '\'' +
                '}';
    }
}
