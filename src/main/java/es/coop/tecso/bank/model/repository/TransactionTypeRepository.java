package es.coop.tecso.bank.model.repository;

import es.coop.tecso.bank.model.dto.enums.TransactionTypeEnum;
import es.coop.tecso.bank.model.entity.TransactionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@SuppressWarnings("unused")
@Repository
public interface TransactionTypeRepository extends JpaRepository<TransactionType, Long> {

    /**
     * @param transactionTypeEnum
     * @return
     */
    Optional<TransactionType> findByName(TransactionTypeEnum transactionTypeEnum);
}
