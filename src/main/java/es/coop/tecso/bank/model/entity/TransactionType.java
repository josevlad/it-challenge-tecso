package es.coop.tecso.bank.model.entity;

import es.coop.tecso.bank.model.dto.enums.TransactionTypeEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "transaction_type")
public class TransactionType implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private TransactionTypeEnum name;

    @OneToMany(mappedBy = "transactionType")
    private Set<Transaction> transactions = new HashSet<>();

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransactionTypeEnum getName() {
        return name;
    }

    public void setName(TransactionTypeEnum name) {
        this.name = name;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    // for MapStruct
    public void setName(String name) {
        this.name = TransactionTypeEnum.valueOf(name);
    }

    /****************************************************************************************************/

    public TransactionType name(TransactionTypeEnum name) {
        this.name = name;
        return this;
    }

    public TransactionType transactions(Set<Transaction> transactions) {
        this.transactions = transactions;
        return this;
    }

    public TransactionType addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
        transaction.setTransactionType(this);
        return this;
    }

//    public TransactionType removeTransaction(Transaction transaction) {
//        this.transactions.remove(transaction);
//        transaction.setTransactionType(null);
//        return this;
//    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TransactionType transactionType = (TransactionType) o;
        if (transactionType.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), transactionType.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TransactionType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
