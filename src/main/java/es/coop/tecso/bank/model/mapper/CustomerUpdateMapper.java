package es.coop.tecso.bank.model.mapper;

import es.coop.tecso.bank.model.dto.CustomerUpdateDTO;
import es.coop.tecso.bank.model.entity.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface CustomerUpdateMapper extends EntityMapper<CustomerUpdateDTO, Customer> {

    @Mapping(target = "bankAccounts", ignore = true)
    Customer toEntity(CustomerUpdateDTO customerUpdateDTO);

    @Mapping(target = "documentType", ignore = true)
    CustomerUpdateDTO toDto(Customer customer);

    default Customer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Customer customer = new Customer();
        customer.setId(id);
        return customer;
    }
}
