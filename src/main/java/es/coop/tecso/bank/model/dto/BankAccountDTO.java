package es.coop.tecso.bank.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "account_code", "account_number", "balance", "account_type", "agency" })
public class BankAccountDTO implements Serializable {

    @ApiModelProperty(notes = ApiDoc.idNote, example = ApiDoc.idExample, position = 0, readOnly = true)
    private Long id;

    @ApiModelProperty(notes = ApiDoc.accountCodeNote, example = ApiDoc.accountCodeExample, position = 1, readOnly = true)
    private String accountCode;

    @ApiModelProperty(notes = ApiDoc.accountNumberExample, example = ApiDoc.accountNumberExample, position = 2, readOnly = true)
    private String accountNumber;

    @ApiModelProperty(notes = ApiDoc.balanceNote, example = ApiDoc.balanceExample, position = 3, readOnly = true)
    private String balance;

    @JsonIgnore
    @ApiModelProperty(hidden = true, readOnly = true)
    private BigDecimal balanceDB;

    @ApiModelProperty(notes = ApiDoc.agencyNote, example = ApiDoc.agencyExample, position = 5, readOnly = true)
    private AgencyDTO agency;

    @ApiModelProperty(notes = ApiDoc.accountTypeNote, example = ApiDoc.accountTypeExample, position = 6, readOnly = true)
    private AccountTypeDTO accountType;

    //private CustomerDTO customer;


    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public AgencyDTO getAgency() {
        return agency;
    }

    public void setAgency(AgencyDTO agency) {
        this.agency = agency;
    }

    public AccountTypeDTO getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountTypeDTO accountType) {
        this.accountType = accountType;
    }

    public BigDecimal getBalanceDB() {
        return balanceDB;
    }

    public void setBalanceDB(BigDecimal balanceDB) {
        this.balanceDB = balanceDB;
    }

//    public CustomerDTO getCustomer() {
//        return customer;
//    }
//
//    public void setCustomer(CustomerDTO customer) {
//        this.customer = customer;
//    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BankAccountDTO bankAccountDTO = (BankAccountDTO) o;
        if (bankAccountDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bankAccountDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BankAccountDTO{" +
                "id=" + id +
                ", accountCode='" + accountCode + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", balanceDB=" + balance +
                ", agency=" + agency +
                ", accountType=" + accountType +
                '}';
    }

    /**************************************************************************************************/

    private interface ApiDoc {
        String idNote = "Represents the primary key<br>";
        String idExample = "1";

        String accountCodeNote = ".<br>";
        String accountCodeExample = "qw";

        String accountNumberNote = ".<br>";
        String accountNumberExample = "USD";
        String allowableValues = "ARG,USD,EUR";

        String balanceNote = ".<br>";
        String balanceExample = "83";

        String balanceDBNote = ".<br>";
        String balanceDBExample = "83";

        String agencyNote = ".<br>";
        String agencyExample = "83";

        String accountTypeNote = ".<br>";
        String accountTypeExample = "83";

    }
}
