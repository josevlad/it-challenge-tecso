package es.coop.tecso.bank.model.repository;

import es.coop.tecso.bank.model.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    /**
     * @return
     */
    List<Transaction> findAllByOrderByTransactionDateDesc();

    /**
     * @param id
     * @return
     */
    List<Transaction> findAllByBankAccountIdOrderByTransactionDateDesc(Long id);
}
