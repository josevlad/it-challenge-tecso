package es.coop.tecso.bank.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;
import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.RegularExpressions.REGEXP_ONLY_NUMBERS;

public class BankAccountCreateDTO implements Serializable {

    @NotBlank(message = NOT_BLANK)
    @JsonProperty("document_number")
    @Pattern(regexp = REGEXP_ONLY_NUMBERS, message = ONLY_NUMBERS)
    @ApiModelProperty(notes = ApiDoc.dnNote, example = ApiDoc.dnExample)
    private String customerDocumentNumber;

    @JsonProperty("account_type_id")
    @NotBlank(message = NOT_BLANK)
    @Pattern(regexp = REGEXP_ONLY_NUMBERS, message = ONLY_NUMBERS)
    @ApiModelProperty(notes = ApiDoc.dtNote, example = ApiDoc.dtExample, position = 1)
    private String accountTypeId;

    @JsonProperty("agency_code")
    @NotBlank(message = NOT_BLANK)
    @Size(message = MAX_SIZE, max = 4)
    @Pattern(regexp = REGEXP_ONLY_NUMBERS, message = ONLY_NUMBERS)
    @ApiModelProperty(notes = ApiDoc.agencyCodeNote, example = ApiDoc.agencyCodeExample, position = 6)
    private String agencyCode;

    /****************************************************************************************************/

    public String getCustomerDocumentNumber() {
        return customerDocumentNumber;
    }

    public void setCustomerDocumentNumber(String customerDocumentNumber) {
        this.customerDocumentNumber = customerDocumentNumber;
    }

    public String getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(String accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccountCreateDTO that = (BankAccountCreateDTO) o;
        return customerDocumentNumber.equals(that.customerDocumentNumber) &&
                accountTypeId.equals(that.accountTypeId) &&
                agencyCode.equals(that.agencyCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerDocumentNumber, accountTypeId, agencyCode);
    }

    @Override
    public String toString() {
        return "BankAccountCreateDTO{" +
                "customerDocumentNumber='" + customerDocumentNumber + '\'' +
                ", accountTypeId='" + accountTypeId + '\'' +
                ", agencyCode='" + agencyCode + '\'' +
                '}';
    }

    /****************************************************************************************************/

    private interface ApiDoc {
        String dnNote = "Represent the customer's document number ID.";
        String dnExample = "95420300";

        String dtNote = "only one of the allowed values is accepted.";
        String dtExample = "2";

        String agencyCodeNote = "Represents the assigned code of the banking agency, it is a 4-digit code.";
        String agencyCodeExample = "0001";
    }
}
