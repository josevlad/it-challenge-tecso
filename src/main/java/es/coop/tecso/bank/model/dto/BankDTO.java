package es.coop.tecso.bank.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import es.coop.tecso.bank.model.entity.Agency;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;
import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.RegularExpressions.REGEXP_ONLY_LETTERS;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "iba_code", "bank_code", "country" })
public class BankDTO implements Serializable {

    private Long id;

    @NotBlank(message = NOT_BLANK)
    @Pattern(regexp = REGEXP_ONLY_LETTERS, message = ONLY_LETTERS)
    @JsonProperty("name")
    private String name;

    @JsonProperty("iba_code")
    private String ibaCode;

    @JsonProperty("bank_code")
    private String bankCode;

    @NotBlank(message = NOT_BLANK)
    @Pattern(regexp = REGEXP_ONLY_LETTERS, message = ONLY_LETTERS)
    @Size(message = MAX_SIZE, max = 2)
    @JsonProperty("country_code")
    private String countryCode;

    @JsonProperty("country")
    private CountryDTO country;

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIbaCode() {
        return ibaCode;
    }

    public void setIbaCode(String ibaCode) {
        this.ibaCode = ibaCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public CountryDTO getCountry() {
        return country;
    }

    public void setCountry(CountryDTO country) {
        this.country = country;
    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BankDTO bankDTO = (BankDTO) o;
        if (bankDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bankDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BankDTO{" +
                "id=" + getId() +
                ", name='" + getName() + "'" +
                ", ibaCode='" + getIbaCode() + "'" +
                ", bankCode='" + getBankCode() + "'" +
                ", countryCode='" + getCountryCode() + "'" +
                "}";
    }
}
