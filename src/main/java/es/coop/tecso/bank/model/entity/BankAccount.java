package es.coop.tecso.bank.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import es.coop.tecso.bank.exception.StandardApplicationException;
import es.coop.tecso.bank.model.dto.enums.CurrencyEnum;
import es.coop.tecso.bank.util.ServicesImplUtil;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;

@Entity
@Table(name = "bank_account")
public class BankAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "account_code")
    private String accountCode;

    @Column(name = "account_number")
    private String accountNumber;

    @Transient
    private String balance;

    @Column(name = "balance", precision = 10, scale = 2)
    private BigDecimal balanceDB;

    @ManyToOne
    @JsonIgnoreProperties("bankAccounts")
    private Agency agency;

    @ManyToOne
    @JsonIgnoreProperties("bankAccounts")
    private AccountType accountType;

    @ManyToOne
    @JsonIgnoreProperties("bankAccounts")
    private Customer customer;

    @OneToMany(mappedBy = "bankAccount")
    private Set<Transaction> transactions = new HashSet<>();

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public BigDecimal getBalanceDB() {
        return balanceDB;
    }

    public void setBalanceDB(BigDecimal balanceDB) {
        this.balanceDB = balanceDB;
    }

    /****************************************************************************************************/

    public BankAccount accountCode(String accountCode) {
        this.accountCode = accountCode;
        return this;
    }

    public BankAccount accountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    public BankAccount balanceDB(BigDecimal balance) {
        this.balanceDB = balance;
        return this;
    }

    public BankAccount agency(Agency agency) {
        this.agency = agency;
        return this;
    }

    public BankAccount accountType(AccountType accountType) {
        this.accountType = accountType;
        return this;
    }

    public BankAccount customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public BankAccount transactions(Set<Transaction> transactions) {
        this.transactions = transactions;
        return this;
    }

    public BankAccount addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
        transaction.setBankAccount(this);
        return this;
    }

    public BankAccount formattBalance() {
        Locale l = ServicesImplUtil.getLocaleByCurrency(accountType.getCurrency());
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(l);
        balance = numberFormat.format(balanceDB);
        return this;
    }

    public BankAccount updateBalanceCredit(BigDecimal amount) {
        balanceDB = balanceDB.add(amount);
        return formattBalance();
    }
    public BankAccount updateBalanceDebit(BigDecimal amount) throws StandardApplicationException {
        FieldError fieldError = null;
        if (balanceDB.compareTo(amount) >= 0) {
            if (accountType.getCurrency().equals(CurrencyEnum.ARG.name()) && amount.compareTo(BigDecimal.valueOf(1000)) == 1)
                fieldError = new FieldError("bankAccount", "amount", LIMIT_EXCEEDED_ARG);

            if (accountType.getCurrency().equals(CurrencyEnum.USD.name()) && amount.compareTo(BigDecimal.valueOf(300)) == 1)
                fieldError = new FieldError("bankAccount", "amount", LIMIT_EXCEEDED_USD);

            if (accountType.getCurrency().equals(CurrencyEnum.EUR.name()) && amount.compareTo(BigDecimal.valueOf(150)) == 1)
                fieldError = new FieldError("bankAccount", "amount", LIMIT_EXCEEDED_EUR);
        } else {
            fieldError = new FieldError("bankAccount", "amount", INSUFFICIENT_BALANCE);
        }

        if (fieldError != null)
            throw new StandardApplicationException(HttpStatus.CONFLICT, fieldError);

        balanceDB = balanceDB.subtract(amount);
        return formattBalance();
    }

//    public BankAccount removeTransaction(Transaction transaction) {
//        this.transactions.remove(transaction);
//        transaction.setBankAccount(null);
//        return this;
//    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BankAccount bankAccount = (BankAccount) o;
        if (bankAccount.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bankAccount.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BankAccount{" +
            "id=" + getId() +
            ", accountCode='" + getAccountCode() + "'" +
            ", accountNumber='" + getAccountNumber() + "'" +
            ", balanceDB=" + getBalance() +
            "}";
    }
}
