package es.coop.tecso.bank.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;
import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.RegularExpressions.REGEXP_ALPHANUMERIC_WITH_SPACE;
import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.RegularExpressions.REGEXP_ONLY_NUMBERS;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "agency_code", "bank" })
public class AgencyDTO implements Serializable {

    private Long id;

    @NotBlank(message = NOT_BLANK)
    @Pattern(regexp = REGEXP_ALPHANUMERIC_WITH_SPACE, message = ALPHANUMERIC)
    @JsonProperty("name")
    private String name;

    @NotBlank(message = NOT_BLANK)
    @Pattern(regexp = REGEXP_ONLY_NUMBERS, message = ONLY_NUMBERS)
    @Size(message = MAX_SIZE, max = 4)
    @JsonProperty("bank_code")
    private String bankCode;

    @JsonProperty("agency_code")
    private String agencyCode;

    @JsonProperty("bank")
    private BankDTO bank;

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public BankDTO getBank() {
        return bank;
    }

    public void setBank(BankDTO bank) {
        this.bank = bank;
    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AgencyDTO agencyDTO = (AgencyDTO) o;
        if (agencyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), agencyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AgencyDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", bankCode='" + bankCode + '\'' +
                ", agencyCode='" + agencyCode + '\'' +
                ", bank=" + bank +
                '}';
    }
}
