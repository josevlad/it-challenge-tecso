package es.coop.tecso.bank.model.repository;

import es.coop.tecso.bank.model.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@SuppressWarnings("unused")
@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {

    /**
     *
     * @param name
     * @return
     */
    Optional<Country> findByCountryCode(String name);
}
