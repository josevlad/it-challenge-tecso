package es.coop.tecso.bank.model.converter;

import es.coop.tecso.bank.model.dto.enums.DocumentTypeEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class DocumentTypeConverter implements AttributeConverter<DocumentTypeEnum, String> {
    @Override
    public String convertToDatabaseColumn(DocumentTypeEnum attribute) {
        return attribute.getDocumentType();
    }

    @Override
    public DocumentTypeEnum convertToEntityAttribute(String dbData) {
        return DocumentTypeEnum.getFromAttributeValue(dbData);
    }
}
