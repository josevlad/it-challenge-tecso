package es.coop.tecso.bank.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.util.Objects;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.*;
import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.RegularExpressions.*;

public class AddTransactionDTO {

    @JsonProperty("description")
    @NotBlank(message = NOT_BLANK)
    @Size(message = MAX_SIZE, max = 200)
    @Pattern(regexp = REGEXP_ALPHANUMERIC_WITH_SPACE, message = ALPHANUMERIC)
    @ApiModelProperty(notes = ApiDoc.descriptionNote, example = ApiDoc.descriptionExample, position = 2)
    private String description;

    @JsonProperty("transaction_type_id")
    @NotBlank(message = NOT_BLANK)
    @Pattern(regexp = REGEXP_ONLY_NUMBERS, message = ONLY_NUMBERS)
    @ApiModelProperty(notes = ApiDoc.transactionTypeIdNote, example = ApiDoc.transactionTypeIdExample, position = 3)
    private String transactionTypeId;

    @JsonProperty("amount")
    @NotBlank(message = NOT_BLANK)
    @Pattern(regexp = REGEXP_TWO_DECIMAL, message = TWO_DECIMAL)
    @ApiModelProperty(notes = ApiDoc.amountNote, example = ApiDoc.amountExample, position = 1)
    private String amount;

    /**************************************************************************************************/

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(String transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddTransactionDTO that = (AddTransactionDTO) o;
        return description.equals(that.description) &&
                transactionTypeId.equals(that.transactionTypeId) &&
                amount.equals(that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, transactionTypeId, amount);
    }

    @Override
    public String toString() {
        return "AddTransactionDTO{" +
                "description='" + description + '\'' +
                ", transactionTypeId='" + transactionTypeId + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }

    public TransactionDTO generateTransactionDTO() {
        return new TransactionDTO()
                .description(description)
                .transactionTypeId(transactionTypeId)
                .amount(amount);
    }

    /**************************************************************************************************/

    private interface ApiDoc {

        String descriptionNote = "details of the bank transaction.<br>";
        String descriptionExample = "Internet subscription payment";

        String amountNote = "Amount of the bank transaction.<br>";
        String amountExample = "275.75";

        String transactionTypeIdNote = "ID of the type of the bank transaction.<br>";
        String transactionTypeIdExample = "2";

    }
}
