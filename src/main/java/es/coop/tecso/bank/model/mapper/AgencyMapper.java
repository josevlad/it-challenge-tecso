package es.coop.tecso.bank.model.mapper;

import es.coop.tecso.bank.model.dto.AgencyDTO;
import es.coop.tecso.bank.model.entity.Agency;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = { BankMapper.class })
public interface AgencyMapper extends EntityMapper<AgencyDTO, Agency> {

    @Mapping(target = "bankCode", ignore = true)
    AgencyDTO toDto(Agency agency);

    @Mapping(target = "bankAccounts", ignore = true)
    Agency toEntity(AgencyDTO agencyDTO);

    default Agency fromId(Long id) {
        if (id == null) {
            return null;
        }
        Agency agency = new Agency();
        agency.setId(id);
        return agency;
    }
}
