package es.coop.tecso.bank.model.mapper;

import es.coop.tecso.bank.model.dto.CustomerDTO;
import es.coop.tecso.bank.model.entity.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {

    @Mapping(target = "bankAccounts", ignore = true)
    Customer toEntity(CustomerDTO customerDTO);

    @Mapping(target = "accountType", ignore = true)
    @Mapping(target = "agencyCode", ignore = true)
    CustomerDTO toDto(Customer customer);

    default Customer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Customer customer = new Customer();
        customer.setId(id);
        return customer;
    }
}
