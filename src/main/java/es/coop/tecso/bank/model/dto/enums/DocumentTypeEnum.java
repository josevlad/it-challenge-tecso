package es.coop.tecso.bank.model.dto.enums;

import com.google.common.collect.Maps;

import java.util.EnumSet;
import java.util.Map;

/**
 * The DocumentTypeEnum enumeration.
 */
public enum DocumentTypeEnum {
    DNI("DNI"),
    LE("LIBRETA DE ENROLAMIENTO"),
    LC("LIBRETA CIVICA"),
    PASSPORT("PASAPORTE");

    private final String documentType;

    private static Map<String, DocumentTypeEnum> documentTypeEnumMap =
            Maps.uniqueIndex(EnumSet.allOf(DocumentTypeEnum.class), DocumentTypeEnum::getDocumentType);

    DocumentTypeEnum(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentType() {
        return documentType;
    }

    public static DocumentTypeEnum getFromAttributeValue(String attributeValue) {
        return documentTypeEnumMap.get(attributeValue);
    }

    @Override
    public String toString() {
        return "DocumentTypeEnum {" +
                "documentType='" + documentType + '\'' +
                " }";
    }
}
