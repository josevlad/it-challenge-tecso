package es.coop.tecso.bank.model.repository;

import es.coop.tecso.bank.model.entity.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("unused")
@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {


    /**
     * @return
     */
    @Query(value = "SELECT ba.* FROM bank_account AS ba ORDER BY ba.id DESC LIMIT 1", nativeQuery = true)
    Optional<BankAccount> findLastInsert();

    /**
     * @param id
     * @return
     */
    List<BankAccount> findByCustomerId(Long id);
}
