package es.coop.tecso.bank.model.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "country")
public class Country implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Size(max = 2)
    @Column(name = "country_code", length = 2)
    private String countryCode;

    @OneToMany(mappedBy = "country")
    private Set<Bank> banks = new HashSet<>();

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Set<Bank> getBanks() {
        return banks;
    }

    public void setBanks(Set<Bank> banks) {
        this.banks = banks;
    }

    /****************************************************************************************************/

    public Country name(String name) {
        this.name = name;
        return this;
    }

    public Country countryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public Country banks(Set<Bank> banks) {
        this.banks = banks;
        return this;
    }

    public Country addBank(Bank bank) {
        this.banks.add(bank);
        bank.setCountry(this);
        return this;
    }

//    public Country removeBank(Bank bank) {
//        this.banks.remove(bank);
//        bank.setCountry(null);
//        return this;
//    }

    /****************************************************************************************************/


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Country country = (Country) o;
        if (country.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), country.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Country{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", countryCode='" + getCountryCode() + "'" +
            "}";
    }
}
