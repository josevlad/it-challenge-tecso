package es.coop.tecso.bank.model.mapper;

import es.coop.tecso.bank.model.dto.TransactionTypeDTO;
import es.coop.tecso.bank.model.entity.TransactionType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface TransactionTypeMapper extends EntityMapper<TransactionTypeDTO, TransactionType> {

    @Mapping(target = "transactions", ignore = true)
    TransactionType toEntity(TransactionTypeDTO transactionTypeDTO);

    default TransactionType fromId(Long id) {
        if (id == null) {
            return null;
        }
        TransactionType transactionType = new TransactionType();
        transactionType.setId(id);
        return transactionType;
    }
}
