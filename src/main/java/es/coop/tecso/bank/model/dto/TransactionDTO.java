package es.coop.tecso.bank.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "transaction_date", "amount", "description", "bank_account" })
public class TransactionDTO implements Serializable {

    @ApiModelProperty(notes = ApiDoc.idNote, example = ApiDoc.idExample, readOnly = true, position = 0)
    private Long id;

    @ApiModelProperty(notes = ApiDoc.transactionDateNote, example = ApiDoc.transactionDateExample, readOnly = true)
    private Timestamp transactionDate;

    @ApiModelProperty(notes = ApiDoc.descriptionNote, example = ApiDoc.descriptionExample, position = 2)
    private String description;

    @ApiModelProperty(notes = ApiDoc.transactionTypeIdNote, example = ApiDoc.transactionTypeIdExample, position = 3)
    private String transactionTypeId;

    @ApiModelProperty(notes = ApiDoc.amountNote, example = ApiDoc.amountExample, position = 1)
    private String amount;

    @ApiModelProperty(hidden = true, readOnly = true)
    private BigDecimal amountDB;

    @ApiModelProperty(notes = ApiDoc.transactionTypeNote, example = ApiDoc.transactionTypeExample, readOnly = true)
    private TransactionTypeDTO transactionType;

    @ApiModelProperty(notes = ApiDoc.bankAccountNote, readOnly = true)
    private BankAccountDTO bankAccount;

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Timestamp transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public BigDecimal getAmountDB() {
        return amountDB;
    }

    public void setAmountDB(BigDecimal amountDB) {
        this.amountDB = amountDB;
    }

    public String getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(String transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public TransactionTypeDTO getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionTypeDTO transactionType) {
        this.transactionType = transactionType;
    }

    public BankAccountDTO getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccountDTO bankAccount) {
        this.bankAccount = bankAccount;
    }

    /****************************************************************************************************/

    public TransactionDTO description(String description) {
        this.description = description;
        return this;
    }

    public TransactionDTO transactionTypeId(String transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
        return this;
    }

    public TransactionDTO amount(String amount) {
        this.amount = amount;
        return this;
    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TransactionDTO transactionDTO = (TransactionDTO) o;
        if (transactionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), transactionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TransactionDTO{" +
                "id=" + getId() +
                ", transactionDate='" + getTransactionDate() + "'" +
                ", description='" + getDescription() + "'" +
                ", amountDB=" + getAmountDB() +
                "}";
    }

    /**************************************************************************************************/

    private interface ApiDoc {
        String idNote = "Represents the primary key<br>";
        String idExample = "1";

        String transactionDateNote = ".<br>";
        String transactionDateExample = "qw";

        String descriptionNote = "details of the bank transaction.<br>";
        String descriptionExample = "Internet subscription payment";

        String amountNote = "Amount of the bank transaction.<br>";
        String amountExample = "275.75";

        String transactionTypeIdNote = "ID of the type of the bank transaction.<br>";
        String transactionTypeIdExample = "2";

        String transactionTypeNote = "Type of bank transaction.<br>";
        String transactionTypeExample = "CRÉDITO";

        String bankAccountNote = "Bank account of the transaction.<br>";

    }
}
