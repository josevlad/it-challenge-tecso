package es.coop.tecso.bank.model.dto.enums;

/**
 * The CurrencyEnum enumeration.
 */
public enum CurrencyEnum {
    ARG("$"),
    EUR("€"),
    USD("$")
    ;

    private final String currency;

    CurrencyEnum(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }
}
