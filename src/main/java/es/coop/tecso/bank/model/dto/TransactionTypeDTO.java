package es.coop.tecso.bank.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import es.coop.tecso.bank.component.validator.enumvalidator.EnumValue;
import es.coop.tecso.bank.model.dto.enums.TransactionTypeEnum;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Objects;

import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.ENUM_VALUES;
import static es.coop.tecso.bank.model.dto.enums.ValidationEnum.Messages.NOT_BLANK;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name" })
public class TransactionTypeDTO implements Serializable {

    private Long id;

    @NotBlank(message = NOT_BLANK)
    @EnumValue(enumClass = TransactionTypeEnum.class, message = ENUM_VALUES + "[CREDIT, DEBIT]")
    @JsonProperty("name")
    private String name;

    /****************************************************************************************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.toUpperCase().trim();
    }

    // for MapStruct
    public void setName(TransactionTypeEnum name) {
        this.name = name.getTransactionType();
    }

    /****************************************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TransactionTypeDTO transactionTypeDTO = (TransactionTypeDTO) o;
        if (transactionTypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), transactionTypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TransactionTypeDTO{" +
                "id=" + getId() +
                ", name='" + getName() + "'" +
                "}";
    }
}
