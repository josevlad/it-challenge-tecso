package es.coop.tecso.bank.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ControllerUtil {

    public static ResponseEntity<?> buildResponser(HttpStatus status, HttpHeaders httpHeaders, Object body) {
        return ResponseEntity.status(status).headers(httpHeaders).body(body);
    }
}
