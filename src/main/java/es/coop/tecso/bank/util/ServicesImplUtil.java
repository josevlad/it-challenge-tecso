package es.coop.tecso.bank.util;

import com.google.common.base.Strings;
import es.coop.tecso.bank.model.entity.AccountType;
import es.coop.tecso.bank.model.entity.BankAccount;
import es.coop.tecso.bank.model.repository.AccountTypeRepository;
import es.coop.tecso.bank.model.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.Normalizer;
import java.util.Locale;
import java.util.Optional;

@Service
public class ServicesImplUtil {

    @Value("${values.default.account-type}")
    private String accountTypeDefault;

    @Lazy
    @Autowired
    @Qualifier("accountTypeRepository")
    private AccountTypeRepository accountTypeRepository;

    @Lazy
    @Autowired
    @Qualifier("bankAccountRepository")
    private BankAccountRepository bankAccountRepository;


    public static boolean isSameString(String stringOne, String stringTwo) {
        stringOne = getNormalizerText(stringOne);
        stringTwo = getNormalizerText(stringTwo);
        return stringOne.equals(stringTwo);
    }

    public static String getNormalizerText(String text) {
        return Normalizer
                .normalize(text.trim(), Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");
    }

    public static Locale getLocaleByCurrency(String currency) {
        switch (currency) {
            case "ARG":
                return new Locale("es", "AR");
            case "USD":
                return new Locale("en", "US");
            case "EUR":
                return new Locale("es", "ES");
            default:
                return new Locale("es", "AR");
        }
    }

    public AccountType getAccountTypeByNameOrId(String nameOrId) {
        Optional<AccountType> accountType = Optional.empty();

        if (nameOrId != null) {
            if (nameOrId.chars().allMatch(Character::isDigit))
                accountType = accountTypeRepository.findById(Long.parseLong(nameOrId));

            else if (nameOrId.chars().allMatch(Character::isAlphabetic))
                accountType = accountTypeRepository.findByName(nameOrId);
        }
        return accountType
                .orElseGet(() -> accountTypeRepository.findById(Long.parseLong(accountTypeDefault)).get());

    }

    public BankAccount generateDataAccount(BankAccount bankAccount) {
        Optional<BankAccount> lastInsert = bankAccountRepository.findLastInsert();

        String zeros = "";
        String accountCode = "0000000001";
        String accountNumber = bankAccount.getAgency().getBank().getIbaCode() + "-" +
                bankAccount.getAgency().getBank().getBankCode() + "-" +
                bankAccount.getAgency().getAgencyCode() + "-" +
                bankAccount.getAccountType().getControlCode();

        if (lastInsert.isPresent()) {
            int lastSaved = Integer.parseInt(lastInsert.get().getAccountCode());
            accountCode = String.valueOf(lastSaved + 1);
            if (String.valueOf(lastSaved).length() < 10)
                zeros = Strings.repeat("0", 10 - String.valueOf(lastSaved).length());
        }

        accountCode = zeros + accountCode;

        accountNumber = accountNumber + "-" + accountCode;

        return bankAccount
                .accountCode(accountCode)
                .accountNumber(accountNumber)
                .balanceDB(new BigDecimal("0.00"))
                .formattBalance();
    }
}
