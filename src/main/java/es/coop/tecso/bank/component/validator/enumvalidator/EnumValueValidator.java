package es.coop.tecso.bank.component.validator.enumvalidator;

import com.google.common.collect.Lists;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EnumValueValidator implements ConstraintValidator<EnumValue, String> {

    private Class<? extends Enum<?>> enumClass;

    @Override
    public void initialize(EnumValue constraintAnnotation) {
        this.enumClass = constraintAnnotation.enumClass();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) return true;

        return Lists.newArrayList(enumClass.getEnumConstants()).stream()
                .map(enumClass::cast)
                .map(Enum::name)
                .anyMatch(enumValue -> enumValue.equals(value.toUpperCase()));
    }
}
